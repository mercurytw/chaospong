package 
{
	import chaos.ActorFactory;
	import chaos.ChaosEvent;
	import chaos.ChaosManager;
	import flash.display.Bitmap;
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.events.KeyboardEvent;
	import flash.events.TextEvent;
	import flash.text.TextField;
	import flash.text.TextFormat;
	import flash.text.TextFormatAlign;
	import flash.text.AntiAliasType;
	import flash.ui.Mouse;
	import flash.events.MouseEvent;
	
	import chaos.Actor;
	import chaos.GameManager;
	import chaos.ITickable;
	import chaos.ResourceManager;
	
	/**
	 * The main class of chaos pong
	 * @author Jeffrey Cochran
	 */
	public class Main extends Sprite 
	{
		private var hasBegun:Boolean = false;
		private const MILLIS_TO_SECONDS_MULTP:Number = 0.001;
		private var isFirstRun:Boolean = true;
		private var lastTick:Date;
		private var gameKeeper:GameManager;
		private var scoreText:TextField;
		private var gameOverText:TextField;
		private var padCtl:PaddleController;
		private var ballCtl:BallController;
		private var startButton:Sprite;
		private var playText:TextField;
		private var titleText:TextField;
		private var circuitText:TextField;
		private var clickToPlay:TextField;
		private var cons:DebugConsole;
		
		[Embed(source="assets/Metrophobic.ttf", fontFamily="foo") ]
		public var bar:String;
		
		public function Main():void 
		{	
			if (stage) init();
			else addEventListener(Event.ADDED_TO_STAGE, init);
		}
		
		private function buildPlayButton():void
		{
			startButton = new Sprite();
			startButton.graphics.beginFill(0x0000FF);
			startButton.graphics.drawRect((stage.width / 2.) - 100., stage.height / 2., 200.0, 50.0);
			startButton.graphics.endFill();
			startButton.buttonMode = true;
			startButton.addEventListener(MouseEvent.CLICK, startPlay);
			
			
			playText = new TextField();
			playText.embedFonts = true;
			playText.antiAliasType = AntiAliasType.ADVANCED;
			playText.selectable = false;
			playText.defaultTextFormat = new TextFormat(ResourceManager.KITCHEN_POLICE, 35.0, 0x000000, null, null, null, null, null, TextFormatAlign.CENTER );
			playText.text = "Play";
			playText.width = stage.stageWidth;
			playText.height = playText.textHeight + 5;
			playText.y =  (stage.height / 2.);
			startButton.addChild( playText );
		}
		
		
		
		private function initText():void
		{
			scoreText = new TextField();
			scoreText.embedFonts = true;
			scoreText.antiAliasType = AntiAliasType.ADVANCED;
			scoreText.selectable = false;
			scoreText.defaultTextFormat = new TextFormat(ResourceManager.METROPHOBIC, stage.stageHeight / 20., 0xFFFFFF, null, null, null, null, null, TextFormatAlign.CENTER );
			scoreText.text = "Score: ";
			scoreText.width = stage.stageWidth;
			scoreText.height = scoreText.textHeight + 5;
			scoreText.y =  0.;
			
			clickToPlay = new TextField();
			clickToPlay.embedFonts = true;
			clickToPlay.antiAliasType = AntiAliasType.ADVANCED;
			clickToPlay.selectable = false;
			clickToPlay.defaultTextFormat = new TextFormat(ResourceManager.KITCHEN_POLICE, stage.stageHeight / 20., 0xFFFFFF, null, null, null, null, null, TextFormatAlign.CENTER );
			clickToPlay.text = "Click to Start";
			clickToPlay.width = stage.stageWidth;
			clickToPlay.height = clickToPlay.textHeight + 5;
			clickToPlay.y =  ( stage.stageHeight / 2.0 ) + 5.0;
			addChild( clickToPlay );
			
			circuitText = new TextField();
			circuitText.embedFonts = true;
			circuitText.antiAliasType = AntiAliasType.ADVANCED;
			circuitText.selectable = false;
			circuitText.defaultTextFormat = new TextFormat(ResourceManager.KITCHEN_POLICE, stage.stageHeight / 20., 0xFFFFFF, null, null, null, null, null, TextFormatAlign.CENTER );
			circuitText.text = "Music by Circuitfry";
			circuitText.width = circuitText.textWidth + 5.;
			circuitText.height = circuitText.textHeight + 5;
			circuitText.y =  stage.stageHeight - circuitText.height;
			addChild( circuitText );
			
			titleText = new TextField();
			titleText.embedFonts = true;
			titleText.antiAliasType = AntiAliasType.ADVANCED;
			titleText.selectable = false;
			titleText.defaultTextFormat = new TextFormat(ResourceManager.KITCHEN_POLICE, stage.stageHeight / 8., 0x010101, null, null, null, null, null, TextFormatAlign.CENTER );
			titleText.text = "Chaos Pong";
			titleText.width = stage.stageWidth;
			titleText.height = titleText.textHeight + 5;
			titleText.y =  50.0;
			addChild( titleText );
			
			gameOverText = new TextField();
			gameOverText.embedFonts = true;
			gameOverText.antiAliasType = AntiAliasType.ADVANCED;
			gameOverText.selectable = false;
			gameOverText.defaultTextFormat = new TextFormat(ResourceManager.METROPHOBIC, stage.stageHeight / 5., 0xFFFFFF, null, null, null, null, null, TextFormatAlign.CENTER );
			gameOverText.text = "Game Over!";
			gameOverText.width = stage.stageWidth;
			gameOverText.height = gameOverText.textHeight + 5;
			gameOverText.y = ((stage.stageHeight / 2.) - (gameOverText.textHeight / 2.) - 125.0);
			gameOverText.rotation = 12.0;
		}
		
		public function restartCallback(event:MouseEvent):void
		{
			gameKeeper.score = 0;
			startButton.removeEventListener(MouseEvent.CLICK, restartCallback);
			removeChild(gameOverText);
			startPlay(event);
		}
		
		private function restartPlay():void
		{
			Mouse.show();
			hasBegun = false;
			addChild(startButton);
			
			isFirstRun = true;
			gameKeeper.isGameOver = false;
			ChaosManager(gameKeeper.chaosMan).reset();
			ballCtl.mPawn.setPos(stage.stageWidth / 2, stage.stageHeight / 2);
			BallController.moveSpeed = BallController.INITIAL_MOVE_SPEED;
			startButton.addEventListener(MouseEvent.CLICK, restartCallback);
		}
		
		private function startPlay(event:MouseEvent):void
		{
			Mouse.hide();
			startButton.removeEventListener(MouseEvent.CLICK, startPlay);
			removeChild(startButton);
			stage.focus = stage;
			gameKeeper.resources.loopBGM();
			
			hasBegun = true;
		}
		
		private function completeInit(event:MouseEvent):void
		{
			stage.removeEventListener(MouseEvent.CLICK, completeInit);
			buildPlayButton();
			
			
			addChild(startButton);
			Sprite(getChildAt( GameManager.BACKGROUND )).addChild( scoreText );
			removeChild( clickToPlay );
			removeChild( titleText );
			removeChild( circuitText );
			
			/*cons = new DebugConsole( gameKeeper );
			
			
			stage.addEventListener( KeyboardEvent.KEY_DOWN, function( keyEvent:KeyboardEvent):void 
															{
																if ( keyEvent.keyCode == 192 ) // tilde. trigger console window
																{
																	cons.toggleConsoleWindow();
																}
															});*/
			gameKeeper.updateList.push( padCtl );
			gameKeeper.updateList.push( ballCtl );
			addEventListener(Event.ENTER_FRAME, tick);
		}
		
		private function init(e:Event = null):void 
		{
			removeEventListener(Event.ADDED_TO_STAGE, init);
		
			gameKeeper = new GameManager( this );
			var fact:ActorFactory = new ActorFactory()
			var paddle:Bitmap = gameKeeper.resources.getBitmap( "paddle" );
			var padAct:Actor = fact.buildActor( paddle, gameKeeper, stage.stageWidth, stage.stageHeight - (paddle.height * 2));
			padCtl = new PaddleController( padAct );	
			gameKeeper.paddleHandle = padAct.uniqueID;
			gameKeeper.paddleState = padCtl;
			
			var ball:Bitmap = gameKeeper.resources.getBitmap( "ball" );
			var ballAct:Actor = fact.buildActor( ball, gameKeeper, stage.stageWidth / 2, stage.stageHeight / 2 );
			ballCtl = new BallController( gameKeeper, gameKeeper.objTable, ballAct, padAct.uniqueID, true );
			gameKeeper.ballHandle = ballAct.uniqueID;
			
			gameKeeper.chaosMan = new ChaosManager( gameKeeper );
			
			
			
			initText();
			stage.addEventListener(MouseEvent.CLICK, completeInit);
		}
		
		/**
		 * The primary game loop
		 * @param	event  should be an ENTER_FRAME event
		 */
		private function tick( event:Event ):void
		{
			if ( !hasBegun )
			{
				return;
			}
			
			var timeInSeconds:Number;
			
			var currentTime:Date = new Date();
			
			if ( gameKeeper.isGameOver && !gameKeeper.infinitePlay )
			{
				return;
			}
			
			if ( isFirstRun )
			{
				timeInSeconds = 0.0;
				isFirstRun = false;
			}
			else
			{
				timeInSeconds = (( currentTime.getTime() - lastTick.getTime() )) * MILLIS_TO_SECONDS_MULTP;
			}
			
			scoreText.text = "Score:  " + gameKeeper.score;
			
			if ( !gameKeeper.shouldSuspendControl )
			{
				gameKeeper.mousePos.x = mouseX;
				gameKeeper.mousePos.y = mouseY;
			}
			
			
			if ( !gameKeeper.pause )
			{
				for each ( var tick:ITickable in gameKeeper.updateList )
				{
					if ( !gameKeeper.pauseKeepItUp || (( tick != padCtl ) && ( tick != ballCtl )) )
					{
						tick.tick( timeInSeconds );
					}
				}
				ChaosManager(gameKeeper.chaosMan).tick( timeInSeconds );
			}
			
			lastTick = currentTime;
			
			if ( gameKeeper.isGameOver && !gameKeeper.infinitePlay )
			{
				addChild(gameOverText);
				gameKeeper.resources.stopBGM();
				restartPlay();
			}
		}
	}
}