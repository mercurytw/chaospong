package  
{
	import chaos.StateMachine;
	import flash.geom.Point; 
	import PaddleBehavior.DetatchedBehavior;
	import PaddleBehavior.KeyboardBehavior;
	import PaddleBehavior.NormalBehavior;
	
	import chaos.Actor;
	import chaos.GameManager;
	import chaos.GameObject;
	import chaos.ITickable;
	
	/**
	 * Handles manipulating the paddle actor
	 * @author Jeff Cochran
	 */
	public class PaddleController extends StateMachine implements ITickable
	{
		public var mPawn:Actor;
		public const speedCap:Number = 100.0;
		public const distThresh:Number = 20.0;
		public function PaddleController( pawn:Actor ) 
		{
			super();
			mPawn = pawn;
			addState( "normal", new NormalBehavior( mPawn, distThresh, speedCap ), true );
			addState( "detatch", new DetatchedBehavior( mPawn ));
			addState( "keyboard", new KeyboardBehavior( mPawn, distThresh, speedCap ));
		}
		
		public function tick( deltaTime:Number ):void
		{
			ITickable(mActiveState).tick( deltaTime );
		}
		
		public function toString():String
		{
			return( "PaddleController" );
		}
	}

}