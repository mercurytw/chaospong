package chaos 
{
	import chaosEvents.touhou.Bullet;
	import flash.display.Bitmap;
	import flash.display.Sprite;
	import flash.events.AccelerometerEvent;
	import flash.geom.Rectangle;
	/**
	 * Builds Actors
	 * @author Jeff Cochran
	 */
	public class ActorFactory 
	{
		
		public function ActorFactory() 
		{
			
		}
		
		/**
		 * Creats an Actor given the Bitmap, GameManager, x,y position, and optional collision volume
		 * @param	drawable a Bitmap to use for this actor. This will eventually be upgraded to a more versatile class later
		 * @param	manager The GameManager shared by this game
		 * @param	xPosition floating point x-position. AS3 ignores precision finer than twips. 
		 * @param	yPosition floating point y-position. AS3 ignores precision finer than twips. 
		 * @param   layer what layer should we add the Actor to?
		 * @param	collisionVolume An optional Rectangle describing the bounding area of this volume. 
		 * 							If none is supplied, one will be inferred from the Bitmap parameter.
		 */
		public function buildActor( drawable:Bitmap, manager:GameManager, xPos:Number, yPos:Number, layer:int=1, collisionVolume:Rectangle=null ):Actor
		{
			var act:Actor = new Actor( drawable, xPos, yPos, collisionVolume );
			manager.objTable.insert( act );
			manager.objTable.insert( act.mVolume );
			act.mVolume.setDrawTarget( Sprite(manager.mainSprite.getChildAt( GameManager.COVER )) );
			drawable.visible = true;
			Sprite(manager.mainSprite.getChildAt( layer )).addChild( act.mDraw );
			manager.colliders.push( act.mVolume );
			manager.updateList.push( act.mVolume );
			return(act);
		}
		
		
		/**
		 * Creats a Bullet given the Bitmap, GameManager, x,y position, and optional collision volume
		 * @param	drawable a Bitmap to use for this actor. This will eventually be upgraded to a more versatile class later
		 * @param	manager The GameManager shared by this game
		 * @param	xPosition floating point x-position. AS3 ignores precision finer than twips. 
		 * @param	yPosition floating point y-position. AS3 ignores precision finer than twips. 
		 * @param   layer what layer should we add the Actor to?
		 * @param	collisionVolume An optional Rectangle describing the bounding area of this volume. 
		 * 							If none is supplied, one will be inferred from the Bitmap parameter.
		 */
		public function buildBullet( drawable:Bitmap, manager:GameManager, xPos:Number, yPos:Number, layer:int=1, collisionVolume:Rectangle=null ):Bullet
		{
			var act:Bullet = new Bullet( drawable, xPos, yPos, collisionVolume );
			manager.objTable.insert( act );
			manager.objTable.insert( act.mVolume );
			act.mVolume.setDrawTarget( Sprite(manager.mainSprite.getChildAt( GameManager.COVER )) );
			Sprite(manager.mainSprite.getChildAt( layer )).addChild( act.mDraw );
			return(act);
		}
		
	}

}