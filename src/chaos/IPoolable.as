package chaos 
{
	
	/**
	 * Interface for a poolable object
	 * @author Jeff Cochran
	 */
	public interface IPoolable 
	{
		function isAlive():Boolean;
		function activate():void;
		function deactivate():void;
	}
	
}