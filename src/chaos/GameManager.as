package chaos
{
	import chaosEvents.TextAdventure;
	import flash.display.Sprite;
	import flash.display.Stage;
	import flash.geom.Point;
	import flash.geom.Rectangle;
	import flash.utils.Dictionary;
	import flash.events.KeyboardEvent;
	/**
	 * Holds shared game state such as the score and update list
	 * @author Jeff Cochran
	 */
	public class GameManager 
	{
		public static const BACKGROUND:int = 0;
		public static const GAME:int = 1;
		public static const COVER:int = 2;
		public static const MARGIN_SIZE:Number = 0.0;//100.0;
		
		public var isGameOver:Boolean; /** has the game ended? */
		public var screenWidth:Number; /** The width of the screen */
		public var screenHeight:Number; /** The height of the screen */
		public var playArea:Rectangle; /** The area of the screen where the ball bounces and the paddle moves */
		public var mousePos:Point = new Point(); /** Where's the mouse? */ 
		public var resources:ResourceManager; /** The resource manager to be used in this game */
		public var updateList:Vector.<ITickable>; /** The list of classes to update with time each frame */
		public var score:int; /** The player's score */
		public var colliders:Vector.<CollisionVolume>; /** The list of collideable objects */
		public var mainSprite:Sprite; /** The root sprite to add drawables to */
		public var objTable:HandleTable; /** The handles of game objects */
		public var pause:Boolean = false; /** Is the game paused? */
		public var pauseKeepItUp:Boolean = false; /** Is keept it up paused? */
		public var chaosMan:StateMachine; /** The chaos manager */
		public var paddleHandle:uint; /** The handle for the paddle actor */
		public var ballHandle:uint; /** The handle for the ball actor */
		public var shouldSuspendControl:Boolean = false; /** should we allow the mouse position to be updated? */
		public var shouldDrawCollisionVolumes:Boolean = false; /** should we draw the collision volumes? */
		public var paddleState:StateMachine; /** The paddle controller as a state machine */
		public var infinitePlay:Boolean = false; /** Should play continue even if the ball hit the bottom of the screen? */
		public function GameManager( main:Sprite ) 
		{
			mainSprite = new Sprite();
			main.addChild(mainSprite);
			screenWidth = main.stage.stageWidth;
			screenHeight = main.stage.stageHeight;
			playArea = new Rectangle( 0., 0., main.stage.stageWidth, main.stage.stageHeight );
			playArea.left = MARGIN_SIZE;
			playArea.right -= MARGIN_SIZE;
			//trace("Width: " + screenWidth.toString());
			//trace("Height: " + screenHeight.toString());
			resources = new ResourceManager();
			updateList = new Vector.<ITickable>();
			colliders = new Vector.<CollisionVolume>();
			isGameOver = false;
			score = 0;
			objTable = new HandleTable();
			
		
			mainSprite.addChildAt( new Sprite(), GameManager.BACKGROUND );
			mainSprite.addChildAt( new Sprite(), GameManager.GAME );
			mainSprite.addChildAt( new Sprite(), GameManager.COVER );
		}
	}

}