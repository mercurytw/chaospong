package chaos 
{
	import flash.events.EventDispatcher;
	/**
	 * A game object is any object in the game. This class is for unique identification of objects.
	 * @author Jeff Cochran
	 */
	public class GameObject extends EventDispatcher
	{
		private static var instances:uint = 0;
		public var uniqueID:uint;
		public var teamID:int;
		public function GameObject( team:int = -1 ) 
		{
			teamID = team;
			uniqueID = generateUniqueID();
		}
		
		private static function generateUniqueID():uint
		{
			return( instances++ );
		}
		
		public override function toString():String
		{
			var str:String = "GameObject#" + uniqueID;
			if ( teamID != -1 )
			{
				str += "#teamID#" + teamID;
			}
			return( str );
		}
	}

}