package chaos 
{
	import chaosEvents.room.Room;
	import flash.ui.KeyboardType;
	import flash.utils.Dictionary;
	/**
	 * ...
	 * @author Jeff Cochran
	 */
	public class StateMachine extends Object
	{
		public var mActiveState:IState;
		public var mStates:Dictionary;
		public var mNames:Vector.<String>;
		public function StateMachine() 
		{
			super();
			mActiveState = null;
			mStates = new Dictionary();
			mStates.toJSON = function (s:String):* {
				var contents = {}; 
				for (var a in mStates) 
				{ 
					var k:String = String(a);
					contents[k] = mStates[k]; 
				}
				return contents;
			}
			mNames = new Vector.<String>();
		}
		
		public static function newStateMachineFromObj(obj:Object, ressurect:Function):StateMachine
		{
			var dict:Object = obj["mStates"];
			var sm:StateMachine = new StateMachine();
			
			
			for ( var key:String in dict )
			{
				var s:IState = ressurect( dict[key] );
				
				sm.addState( key, s );
			}
			return( sm );
		}
		
		public function enterState( name:String ):void
		{
			if (mStates[name] == null)
			{
				trace("StateMachine.enterState: Requested nonexistant state: " + name );
				return;
			}
			if ( mActiveState != null )
			{
				mActiveState.teardown();
			}
			mActiveState = mStates[name];
			mActiveState.setup();
		}
		
		public function addState( name:String, state:IState, isEntryState:Boolean = false ):void
		{
			mStates[name] = state;
			mNames.push( name );
			if (isEntryState)
			{
				enterState(name);
			}
		}
		
		public function toJSON(s:String):*
		{
			return { "mStates":mStates };
		}
	}

}