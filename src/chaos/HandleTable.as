package chaos 
{
	import flash.utils.Dictionary;
	/**
	 * A table of game objects for indirect interaction between objects & avoiding circular dependence.
	 * @author Jeff Cochran
	 */
	public class HandleTable 
	{
		private var mTable:Dictionary;
		public function HandleTable()
		{
			mTable = new Dictionary();
		}
		
		public function insert( object:GameObject ):void
		{
			if ( mTable[object.uniqueID] == null )
			{
				mTable[object.uniqueID] = object;
			}
		}
		
		public function lookup( id:uint ):GameObject
		{
			return( mTable[id] );
		}
		
		public function remove( id:uint ):void
		{
			delete mTable[id];
		}
	}
}