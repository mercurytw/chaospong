package chaos 
{
	import flash.display.Bitmap;
	import flash.events.Event;
	import flash.media.Sound;
	import flash.media.SoundChannel;
	import flash.utils.Dictionary;
	/**
	 * Embeds used assets and provides them as their appropriate classes.
	 * @author Jeff Cochran
	 */
	public class ResourceManager 
	{
		[Embed(source = "../assets/Metrophobic.ttf", fontName = "Metrophobic", mimeType = "application/x-font", 
		fontWeight = "normal", unicodeRange = "U+0020-U+002F,U+0030-U+0039,U+003A-U+0040,U+0041-U+005A,U+005B-U+0060,U+0061-U+007A,U+007B-U+007E", 
		advancedAntiAliasing="true", embedAsCFF="false")]
		public static const METROPHOBIC_FONT:String;

		public static const METROPHOBIC:String = "Metrophobic";
		
		[Embed(source = "../assets/KitchenPolice.ttf", fontName = "KitchenPolice", mimeType = "application/x-font", 
		fontWeight = "normal", unicodeRange = "U+0020-U+002F,U+0030-U+0039,U+003A-U+0040,U+0041-U+005A,U+005B-U+0060,U+0061-U+007A,U+007B-U+007E", 
		advancedAntiAliasing="true", embedAsCFF="false")]
		public static const KITCHEN_POLICE_FONT:String;

		public static const KITCHEN_POLICE:String = "KitchenPolice";
		
		[Embed(source = "../assets/bakaGaijin.mp3")]
		private const bgmClass:Class;
		private var bgm:Sound;
		private var musicChannel:SoundChannel;
		
		[Embed(source = "../assets/close.mp3")]
		private const closeClass:Class;
		private var close:Sound;
		private var sfxChannel:SoundChannel;
		
		
		[Embed(source = "../assets/far.mp3")]
		private const farClass:Class;
		private var far:Sound;


		private var mBitmaps:Dictionary; /** String to Class map */
		public function ResourceManager() 
		{
			mBitmaps = new Dictionary();
			
			
			bgm = new bgmClass();
			far = new farClass();
			
			close = new closeClass();
			
			[Embed(source="../assets/ball.jpg")]
			var pinkClass:Class;
			mBitmaps["ball"] = pinkClass;
			
			[Embed(source="../assets/paddle.jpg")]
			var redClass:Class;
			mBitmaps["paddle"] = redClass;
			
			[Embed(source = "../assets/redBall.jpg")]
			var rdClass:Class;
		    mBitmaps["redBall"] = rdClass;
			
			[Embed(source = "../assets/greyBall.jpg")]
			var greyClass:Class;
			mBitmaps["greyBall"] = greyClass;
			
			[Embed(source = "../assets/face.png")]
			var faceClass:Class;
			mBitmaps["face"] = faceClass;
			
			[Embed(source = "../assets/face2.png")]
			var face2Class:Class;
			mBitmaps["face2"] = face2Class;
			
			[Embed(source = "../assets/face3.png")]
			var face3Class:Class;
			mBitmaps["face3"] = face3Class;
			
			[Embed(source = "../assets/cowboy.png")]
			var cowboyClass:Class;
			mBitmaps["cowboy"] = cowboyClass;
			
			[Embed(source = "../assets/worm.png")]
			var wormClass:Class;
			mBitmaps["worm"] = wormClass;
			
			[Embed(source = "../assets/down.png")]
			var downClass:Class;
			mBitmaps["down"] = downClass;
			
			[Embed(source = "../assets/left.png")]
			var leftClass:Class;
			mBitmaps["left"] = leftClass;
			
			[Embed(source = "../assets/right.png")]
			var rightClass:Class;
			mBitmaps["right"] = rightClass;
			
			[Embed(source = "../assets/up.png")]
			var upClass:Class;
			mBitmaps["up"] = upClass;
			
			[Embed(source = "../assets/guyNeutral.png")]
			var guyNClass:Class;
			mBitmaps["guyNeutral"] = guyNClass;
			
			[Embed(source = "../assets/guyRight.png")]
			var guyRClass:Class;
			mBitmaps["guyRight"] = guyRClass;
			
			[Embed(source = "../assets/guyDown.png")]
			var guyDClass:Class;
			mBitmaps["guyDown"] = guyDClass;
			
			[Embed(source = "../assets/guyLeft.png")]
			var guyLClass:Class;
			mBitmaps["guyLeft"] = guyLClass;
			
			[Embed(source = "../assets/guyUp.png")]
			var guyUClass:Class;
			mBitmaps["guyUp"] = guyUClass;
		}
		
		public function getBitmap( name:String ):Bitmap
		{
			if ( mBitmaps[name] ) // if it's in the dictionary
			{
				return new mBitmaps[name]();
			}
			return null;
		}
		
		public function playSfx( name:String ):void
		{
			if ( name == "close" )
			{
				sfxChannel = close.play();
			}
			else if ( name == "far" )
			{
				sfxChannel = far.play();
			}
		}
		
		private var isPlaying:Boolean;
		public function loopBGM():void
		{
			musicChannel = bgm.play();
			musicChannel.addEventListener(Event.SOUND_COMPLETE, loopHandler);
			isPlaying = true;
		}
		
		public function stopBGM():void
		{
			if ( isPlaying )
			{
				isPlaying = false;
				musicChannel.removeEventListener(Event.SOUND_COMPLETE, loopHandler);
				musicChannel.stop();
			}
		}
		
		public function loopHandler(event:Event):void
		{
			if (musicChannel != null)
			{
				musicChannel.removeEventListener(Event.SOUND_COMPLETE, loopHandler);
				loopBGM();
			}
		}
	}

}