package chaos 
{
	import flash.geom.Point;
	/**
	 * Simple 2D Vector
	 * @author Jeff Cochran
	 */
	public class Vector2D extends Object
	{
		public static const X_AXIS:Vector2D = new Vector2D( 1., 0.);
		public static const Y_AXIS:Vector2D = new Vector2D( 0., 1.);
		public static const UNIT_ZERO:Vector2D = new Vector2D();
		
		private var _length:Number; 
		private var _lengthSquared:Number;
		private var _x:Number;
		private var _y:Number;
		
		public function Vector2D( x:Number = 0., y:Number = 0. ) 
		{
			_length = Number.NaN;
			_lengthSquared = Number.NaN;
			_x = x;
			_y = y;
		}
		
		public static function toVect2D( pt:Point ):Vector2D
		{
			return( new Vector2D( pt.x, pt.y ) );
		}
		
		/** The length, magnitude, of the current Vector2D object from the origin (0,0) to the object's x and ycoordinates. */
		public function get length():Number
		{
			if ( isNaN(_length) )
			{
				_length = Math.sqrt( this.lengthSquared );
			}
			return( _length );
		}
		
		public function get lengthSquared():Number
		{
			if ( isNaN(_lengthSquared) )
			{
				_lengthSquared = Math.pow( _x, 2.0 ) + Math.pow( _y, 2.0 );
			}
			return( _lengthSquared );
		}
		
		public function get x():Number
		{
			return( _x );
		}
		
		public function set x( val:Number ):void
		{
			_length = Number.NaN;
			_lengthSquared = Number.NaN;
			_x = val;
		}
		
		public function get y():Number
		{
			return( _y );
		}
		
		public function set y( val:Number ):void
		{
			_length = Number.NaN;
			_lengthSquared = Number.NaN;
			_y = val;
		}
		
		public function add( a:Vector2D ):Vector2D
		{
			return (new Vector2D( _x + a.x, _y + a.y));
		}
		
		public function dotProduct( a:Vector2D ):Number
		{
			return ( _x * a.x ) + ( _y * a.y );
		}
		
		/**
		 * Normalizes `this` Vector2D to length 1.0
		 * @return A number representing the length of the Vector2D before normalization
		 */
		public function normalize():Number
		{
			var oldLength:Number = this.length;
			var oldy:Number = this.y;
			this.x = _x / oldLength;
			this.y = oldy / oldLength;
			_length = 1.0;
			_lengthSquared = 1.0;
			return oldLength;
		}
		
		public static function angleBetween( a:Vector2D, b:Vector2D ):Number
		{
			if ( a.y < b.y )
			{
				return (Math.acos( ( a.dotProduct(b) / (a.length * b.length)  ) ) * ( 180. / Math.PI));
			}
			return 360. - (Math.acos( ( a.dotProduct(b) / (a.length * b.length)  ) ) * ( 180. / Math.PI));
		}
		
		public function clone():Vector2D
		{
			return new Vector2D( _x, _y );
		}
		
			
		public function copyFrom( sourceVector2D:Vector2D ):void
		{
			this.x = sourceVector2D.x;
			this.y = sourceVector2D.y;
		}
		
		public function decrementBy( a:Vector2D ):void
		{
			this.x -= a.x;
			this.y -= a.y;
		}
		
		public static function distance( a:Vector2D, b:Vector2D ):Number
		{
			return Math.sqrt( Math.pow( b.x - a.x, 2.0) + Math.pow( b.y - a.y, 2.0));
		}
		
		public function equals( a:Vector2D ):Boolean
		{
			return( (_x == a.x) && (_y == a.y))
		}
		
		public function incrementBy( a:Vector2D ):void
		{
			this.x += a.x;
			this.y += a.y;
		}
			
		public function nearEquals( toCompare:Vector2D, tolerance:Number ):Boolean
		{
			return (Math.abs( _x - toCompare.x) + Math.abs( _y - toCompare.y )) <= tolerance;
		}
		
		public function negate():void
		{
			_x = -_x;
			_y = -_y;
		}
		
		public function scaleBy( s:Number ):void
		{
			this.x *= s;
			this.y *= s;
		}
		
		public function setTo( xa:Number, ya:Number ):void
		{
			this.x = xa;
			this.y = ya;
		}
		
		public function subtract( a:Vector2D ):Vector2D
		{
			return new Vector2D( _x - a.x, _y - a.y );
		}
		
		public function fromPoint( a:Point ):Vector2D
		{
			return new Vector2D( a.x, a.y );
		}
		
		public function toString():String
		{
			return "{  " + _x + ", " + _y + " }";
		}
		
		public function crossProduct( b:Vector2D ):Number
		{
			var res:Number = ( this.x * b.y ) - ( this.y * b.x);
			return( res );
		}
		
	}

}