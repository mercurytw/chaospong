package chaos 
{
	import chaos.event.TextInputEvent;
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.events.FocusEvent;
	import flash.events.KeyboardEvent;
	import flash.text.TextField;
	import flash.text.TextFormat;
	//TextField.multiline = true; wordwrap = true;
	/**
	 * a console
	 * @author Jeff Cochran
	 */
	public class Console extends Sprite 
	{
		private var mKeeper:GameManager;
		private var windowText:TextField = new TextField();
		private var inputText:TextField = new TextField();
		
		private var consoleSize:int;
		private const KC_ENTER:uint = 13;
		private const KC_SHIFT:uint = 16;
		private const KC_A:uint = 65;
		private const KC_Z:uint = 90;
		private const KC_ZERO:uint = 48;
		private const KC_NINE:uint = 57;
		private const KC_BKSP:uint = 8;
		private const KC_SP:uint = 32;
		private const FONT_SIZE:Number = 25.0;
		private var isShiftDown:Boolean = false;
		
		public function Console( manager:GameManager ) 
		{
			super();
			mKeeper = manager;
			this.visible = false;
			this.graphics.beginFill( 0x000000, 0.8 );
			this.graphics.drawRect( 0., 0., mKeeper.screenWidth, mKeeper.screenHeight );
			this.graphics.endFill();
			this.width = mKeeper.screenWidth;
			this.height = mKeeper.screenHeight;
			
			initText();
			Sprite(mKeeper.mainSprite.getChildAt( GameManager.COVER )).addChild( this );
		}
		
		private function initText():void
		{
			var fmt:TextFormat = windowText.getTextFormat();
			fmt.size = FONT_SIZE;
			fmt.color = 0xFFFFFF;
			fmt.align = "left";
			consoleSize = Math.floor( mKeeper.screenHeight / Number(fmt.size) );
			
			windowText.defaultTextFormat = fmt;
			windowText.wordWrap = true;
			windowText.multiline = true;
			windowText.text = "";
			windowText.width = this.width + 2.;
			windowText.height = consoleSize * Number(fmt.size);
			windowText.y = 0.;
			this.addChild( windowText );
			
			fmt = inputText.getTextFormat();
			fmt.size = FONT_SIZE;
			fmt.color = 0xFFFFFF;
			fmt.align = "left";
			inputText.defaultTextFormat = fmt;
			inputText.text = "> ";
			inputText.width = this.width + 2.;
			inputText.y = ( Number(fmt.size) + 1. ) * (consoleSize - 2);
			this.addChild( inputText );
			
		}
		
		/**
		 * Append text to the console
		 * @param	output String to show to the user
		 */
		public function write( output:String ):void
		{
			windowText.appendText( output );
		}
		
		/**
		 * Clears the console
		 */
		public function clear():void
		{
			windowText.text = "";
		}
		
		/**
		 * Set the text shown in the input line. This text will dissapear as soon as the user presses a key
		 * @param	output Text to show the user
		 */
		public function setInputLineText( output:String ):void
		{
			inputText.text = "> " + output;
		}
		
		private function perform( cmd:String ):void
		{	
			inputText.text = "";
			this.dispatchEvent(new TextInputEvent(TextInputEvent.TEXT_INPUT, cmd.toLowerCase()) );
		}
		
		private var buffer:String = "";
		public function registerKeyEvent( keyEvent:KeyboardEvent ):void
		{
			if ( ( ( KC_A <= keyEvent.keyCode ) && ( keyEvent.keyCode <= KC_Z ) ) ||
				 ( ( KC_ZERO <= keyEvent.keyCode ) && ( keyEvent.keyCode <= KC_NINE ) ) ||
				 ( keyEvent.keyCode == KC_SP ) )
			{
				buffer += String.fromCharCode(keyEvent.keyCode).toLowerCase();
			}
			else if ( keyEvent.keyCode == KC_BKSP )
			{
				buffer = buffer.substring( 0, buffer.length - 1 );
			}
			else if ( keyEvent.keyCode == KC_ENTER )
			{
				perform(buffer);
				buffer = "";
			}
			inputText.text = "> " + buffer.toLowerCase();
		}
		
		/**
		 * Activates or deactivates this console
		 */
		public function setup():void
		{
			this.visible = true;
			
			buffer = "";
			mKeeper.mainSprite.stage.addEventListener( KeyboardEvent.KEY_DOWN, registerKeyEvent );
			this.addEventListener( FocusEvent.FOCUS_IN, handleFocusEvent );
		}
		
		public function teardown():void
		{
			this.visible = false;
			mKeeper.mainSprite.stage.removeEventListener( KeyboardEvent.KEY_DOWN, registerKeyEvent );
			this.removeEventListener( FocusEvent.FOCUS_IN, handleFocusEvent );
		}
		
		public function handleFocusEvent( event:FocusEvent ):void
		{
			mKeeper.mainSprite.stage.focus = mKeeper.mainSprite.stage;
		}
	}

}