package chaos.event 
{
	import flash.events.Event;
	
	/**
	 * Text input from, say, a console
	 * @author Jeff Cochran
	 */
	public class TextInputEvent extends Event 
	{
		public static const TEXT_INPUT:String = "textInput";
		
		public var message:String;
		public function TextInputEvent(type:String, message:String, bubbles:Boolean=false, cancelable:Boolean=false) 
		{ 
			super(type, bubbles, cancelable);
			this.message = message;
		} 
		
		public override function clone():Event 
		{ 
			return new TextInputEvent(type, message, bubbles, cancelable);
		} 
		
		public override function toString():String 
		{ 
			return formatToString("TextInputEvent", "type", "message", "bubbles", "cancelable", "eventPhase"); 
		}
		
	}
	
}