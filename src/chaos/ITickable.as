package chaos
{
	/**
	 * The ITickable interface represents a class that can be updated with time since
	 * the last frame
	 * @author Jeff Cochran
	 */
	public interface ITickable 
	{
		function tick( deltaTime:Number ):void;
	}

}