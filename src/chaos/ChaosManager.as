package chaos 
{
	import chaosEvents.DetatchEvent;
	import chaosEvents.ImageEvent;
	import chaosEvents.InversionEvent;
	import chaosEvents.KeyControlEvent;
	import chaosEvents.MathEvent;
	import chaosEvents.MultiballEvent;
	import chaosEvents.RhythmEvent;
	import chaosEvents.TextAdventure;
	import chaosEvents.TouhouEvent;
	import flash.display.Sprite;
	import flash.geom.Rectangle;
	/**
	 * The class that's in charge of triggering and storing chaos events. It is a state machine
	 * @author Jeff Cochran
	 */
	public class ChaosManager extends StateMachine implements ITickable
	{
		private static const TIME_BETWEEN_EVENTS:Number = 10.0;
		private static const DIFFICULTY_TIME_STEP:Number = 20.0;
		private static const EVENT_DELAY_THRESH:Number = 0.9;
		private var currentTier:int = 0;
		private var mStoredEvent:String = null;
		public var mKeeper:GameManager; /** The game manager */
		public function ChaosManager( manager:GameManager ) 
		{
			super();
			mKeeper = manager;
			
			addState( "none", new ChaosEvent(), false );
			addState( "multiball", new MultiballEvent( manager ));
			addState( "inversion", new InversionEvent( manager ));
			addState( "math", new MathEvent( manager ));
			addState( "detatch", new DetatchEvent( manager ));
			addState( "image", new ImageEvent( manager ));
			addState( "rhythm", new RhythmEvent( manager));
			addState( "touhou", new TouhouEvent( manager ));
			addState( "text", new TextAdventure( manager ));
			addState( "keyboard", new KeyControlEvent( manager ));
			invokeEvent( "none" );
		}
		
		/**
		 * Enters the given state if the ball isn't too close to the bottom of the screen.
		 * @param	evt The name of the event to trigger
		 * @return  A Boolean indicating whether or not the event was triggered
		 */
		public function invokeEvent( evt:String = null ):Boolean
		{	
			if ( evt != null )
			{
				mStoredEvent = evt;
			}
			if ( mStoredEvent != null )
			{
				var ball:Actor = Actor(mKeeper.objTable.lookup( mKeeper.ballHandle ));
				
				if ( ball.getPos().y <= ( mKeeper.screenHeight * ChaosManager.EVENT_DELAY_THRESH ) )
				{
					this.enterState( mStoredEvent );
					mStoredEvent = null;
					return( true );
				}
			}
			return( false );
		}
		
		
		private function invokeRandomEvent():void
		{
			var index:int;
			do
			{
				index = int(Math.round(Math.random() * (mNames.length - 1)));
			} while ( (ChaosEvent(mStates[mNames[index]]).difficultyTier > currentTier ) && (mNames[index] != "none"));
			invokeEvent( mNames[index] );
		}
		
		private var agg:Number = 0;
		private var inEvent:Boolean = false;
		private var countdown:Number = TIME_BETWEEN_EVENTS;
		public function tick( deltaTime:Number ):void
		{
			inEvent = invokeEvent();
			if (! mKeeper.pauseKeepItUp )
			{
				agg += deltaTime;
			}
			currentTier = int(Math.floor(agg / DIFFICULTY_TIME_STEP));
			
			if ( mActiveState )
			{
				ChaosEvent(mActiveState).tick( deltaTime );
				if ( mActiveState == mStates["none"] )
				{
					countdown -= deltaTime;
					if ( countdown <= 0.0 )
					{
						invokeRandomEvent();
						//invokeEvent( "keyboard" );
					}
				}
				else
				{
					countdown = TIME_BETWEEN_EVENTS;
				}
			}
		}
		
		public function reset():void
		{
			agg = 0;
			invokeEvent("none");
			inEvent = false;
			currentTier = 0;
			countdown = TIME_BETWEEN_EVENTS;
		}
	}

}