package chaos 
{
	/**
	 * A state that can be used by a StateMachine
	 * @author Jeff Cochran
	 */
	public interface IState
	{
		function setup():void;
		function teardown():void;
	}

}