package chaos 
{
	/**
	 * The template for a ChaosEvent. not to be instantiated directly
	 * @author Jeff Cochran
	 */
	public class ChaosEvent extends GameObject implements IState, ITickable 
	{
		public var eventTime:Number;
		public var difficultyTier:int;
		public function ChaosEvent() 
		{
			super();
		}
		
		public function setup():void
		{
			
		}
		
		public function teardown():void
		{
			
		}
		
		public function tick( deltaTime:Number ):void
		{
			
		}
	}

}