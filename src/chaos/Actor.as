package chaos 
{
	import adobe.utils.CustomActions;
	import flash.display.Bitmap;
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.events.EventDispatcher;
	import flash.geom.Point;
	import flash.geom.Rectangle;
	import flash.globalization.Collator;
	
	/**
	 * Actor clas which represents a "physical" object in the game world. Standard actor-controller model
	 * @author Jeff Cochran
	 */
	public class Actor extends GameObject
	{
		public var mVelocity:Vector2D = new Vector2D();
		public var mVolume:CollisionVolume; /**  the collision volume representing the borders of this Actor */
		private var mPosition:Point = new Point(); /** the x,y coordinates of the actor relative to the CENTER of the actor*/
		public var mDraw:Bitmap; /** the drawable held by this actor */
		public var mInitialPosition:Point;
		/**
		 * Creats an Actor given the Bitmap, GameManager, x,y position, and optional collision volume
		 * @param	drawable a Bitmap to use for this actor. This will eventually be upgraded to a more versatile class later
		 * @param	xPosition floating point x-position. AS3 ignores precision finer than twips. 
		 * @param	yPosition floating point y-position. AS3 ignores precision finer than twips. 
		 * @param	collisionVolume An optional Rectangle describing the bounding area of this volume. 
		 * 							If none is supplied, one will be inferred from the Bitmap parameter.
		 */
		public function Actor( drawable:Bitmap, xPosition:Number = 0.0, yPosition:Number = 0.0, collisionVolume:Rectangle = null ) 
		{
			super();
			mDraw = drawable;
			mDraw.visible = false;
			
			mPosition.x = xPosition;  
			mPosition.y = yPosition;
			mInitialPosition = mPosition.clone();
			mDraw.x = xPosition - (mDraw.width / 2.0);
			mDraw.y = yPosition - (mDraw.height / 2.0);
			if ( collisionVolume != null )
			{
				mVolume = new CollisionVolume(collisionVolume.x, collisionVolume.y, collisionVolume.width, collisionVolume.height);
			}
			else
			{
				mVolume = new CollisionVolume();
				mVolume.setTo( xPosition - (mDraw.width / 2.),
							   yPosition - (mDraw.height / 2.),
							   mDraw.getRect(mDraw.parent).width,
							   mDraw.getRect(mDraw.parent).height );
			}
		}
		
		/**
		 * Set the teamID of this actor and it's collision volume
		 * @param	team A Number representing the team.
		 */
		public function setTeam( team:Number ):void
		{
			teamID = team;
			mVolume.teamID = team;
		}
		
		public function getPos():Point
		{
			return mPosition.clone();
		}
		
		public function setPos( x:Number, y:Number ):void
		{
			mPosition.x = x;
			mPosition.y = y;
			mDraw.x = x - (mDraw.width / 2.0);
			mDraw.y = y - (mDraw.height / 2.0);
			mVolume.x = x - (mVolume.width / 2.0);
			mVolume.y = y - (mVolume.height / 2.0);
		}
		
		public function set pos( a:Point ):void
		{
			setPos( a.x, a.y );
		}
		
		public function move( x:Number, y:Number ):void
		{
			setPos( mPosition.x + x, mPosition.y + y );
		}
		
		public override function toString():String
		{
			var str:String = "Actor#" + uniqueID + "#Team" + teamID + "{\n";
			str += "   Position: { " + mPosition.x + ", " + mPosition.y + "}\n";
			str += "   Volume: { " + JSON.stringify( mVolume ) + " }\n}\n"
			return( str );
		}
		
	}

}