package chaos 
{
	import flash.display.DisplayObject;
	import flash.display.Sprite;
	import flash.geom.Point;
	import flash.geom.Rectangle;
	
	/**
	 * Simple rectangular collision volume.
	 * @author Jeff Cochran
	 */
	public class CollisionVolume extends GameObject implements ITickable
	{
		private var mVisible:Boolean = false;
		private const DRAW_COLOR:uint = 0x191919;
		public var mRect:Rectangle;
		private var mDisp:Sprite = new Sprite();
		public function CollisionVolume(x:Number = 0, y:Number = 0, width:Number = 0, height:Number = 0) 
		{
			super();
			mRect = new Rectangle(x, y, width, height);
		}
		
		public function set bottom( a:Number ):void
		{
			mRect.bottom = a;
		}
		
		public function get bottom():Number
		{
			return(mRect.bottom);
		}
		
		public function set bottomRight( a:Point ):void
		{
			mRect.bottomRight = a;
		}
		
		public function get bottomRight():Point
		{
			return(mRect.bottomRight);
		}
		
		public function set height(a:Number):void
		{
			mRect.height = a;
		}
		
		public function get height():Number
		{
			return(mRect.height);
		}
		
		public function set left( a:Number ):void
		{
			mRect.left = a;
		}
		
		public function get left():Number
		{
			return(mRect.left);
		}
		
		public function set right( a:Number ):void
		{
			mRect.right = a;
		}
		
		public function get right():Number
		{
			return mRect.right;
		}
		
		public function set size( a:Point ):void
		{
			mRect.size = a;
		}
		
		public function get size():Point
		{
			return mRect.size;
		}
		
		public function set top( a:Number ):void
		{
			mRect.top = a;
		}
		
		public function get top():Number
		{
			return mRect.top;
		}
		
		public function set topLeft( a:Point ):void
		{
			mRect.topLeft = a;
		}
		
		public function get topLeft():Point
		{
			return mRect.topLeft;
		}
		
		public function set width( a:Number ):void
		{
			mRect.width = a;
		}
		
		public function get width():Number
		{
			return mRect.width;
		}
		
		public function set x( a:Number ):void
		{
			mRect.x = a;
			
		}
		
		public function get x():Number
		{
			return mRect.x;
		}
		
		public function set y( a:Number ):void
		{
			mRect.y = a;
		}
		
		public function get y():Number
		{
			return mRect.y;
		}
		
		public function set visible( a:Boolean ):void
		{
			mVisible = a;
			mDisp.visible = a;
		}
		
		public function get visible():Boolean
		{
			return mVisible;
		}
		
		public function clone():CollisionVolume
		{
			return new CollisionVolume(x, y, width, height);
		}
		
		public function contains(x:Number, y:Number):Boolean
		{
			return mRect.contains(x, y);
		}
		
		public function containsPoint( point:Point ):Boolean
		{
			return mRect.containsPoint( point);
		}
		
		public function containsRect( rect:Rectangle ):Boolean
		{
			return mRect.containsRect(rect);
		}
		
		public function containsVolume( vol:CollisionVolume ):Boolean
		{
			return mRect.containsRect( new Rectangle(vol.x, vol.y, vol.width, vol.height ));
		}
		
		public function toRect():Rectangle
		{
			return new Rectangle( mRect.x, mRect.y, mRect.width, mRect.height );
		}
		
		public function equalsRect(toCompare:Rectangle):Boolean
		{
			return mRect.equals(toCompare);
		}
		
		public function equals(toCompare:CollisionVolume):Boolean
		{
			return mRect.equals(toCompare.toRect());
		}
		
		public function inflate(dx:Number, dy:Number):void
		{
			mRect.inflate(dx, dy);
		}
		
		public function inflatePoint(point:Point):void
		{
			mRect.inflatePoint(point);
		}
		
		public function intersectionRect(toIntersect:Rectangle):Rectangle
		{
			return mRect.intersection(toIntersect);
		}
		
		public function intersection(toIntersect:CollisionVolume):CollisionVolume
		{
			var blah:CollisionVolume = new CollisionVolume();
			var rct:Rectangle = mRect.intersection(toIntersect.mRect);
			blah.setTo(rct.x, rct.y, rct.width, rct.height);
			return blah;
		}
		
		public function intersectsRect(toIntersect:Rectangle):Boolean
		{
			return mRect.intersects(toIntersect);
		}
		
		public function intersects(toIntersect:CollisionVolume):Boolean
		{
			return mRect.intersects(toIntersect.toRect());
		}
		
		public function isEmpty():Boolean
		{
			return mRect.isEmpty();
		}
		
		public function offset(dx:Number, dy:Number):void
		{
			mRect.offset(dx, dy);
		}
		
		public function offsetPoint(point:Point):void
		{
			mRect.offsetPoint(point);
		}
		
		public function setEmpty():void
		{
			mRect.setEmpty();
		}
		
		public function setTo(xa:Number, ya:Number, widtha:Number, heighta:Number):void
		{
			mRect.x = xa;
			mRect.y = ya;
			mRect.width = widtha;
			mRect.height = heighta;
		}
		
		public override function toString():String
		{
			return "CollisionVolume#"  + uniqueID + "#Team" + teamID;
		}
		
		public function union(toUnion:CollisionVolume):CollisionVolume
		{
			var vol:CollisionVolume = new CollisionVolume();
			var rct:Rectangle = mRect.union(toUnion.toRect());
			vol.setTo(rct.x, rct.y, rct.width, rct.height);
			return(vol);
		}
		
		public function setDrawTarget( target:Sprite ):void
		{
			removeDrawTarget();
			target.addChild( mDisp );
		}
		
		public function removeDrawTarget( ):void
		{
			if ( mDisp.parent != null )
			{
				mDisp.parent.removeChild( mDisp );
			}
		}
		
		public function tick( deltaTime:Number ):void
		{
			if ( mVisible && ( mDisp.parent != null ) )
			{
				mDisp.graphics.clear();
				mDisp.graphics.beginFill(DRAW_COLOR, 0.3);
				/*mDisp.graphics.lineStyle( 3., DRAW_COLOR, 0.);
				mDisp.graphics.lineTo(mRect.x, mRect.y);
				mDisp.graphics.lineStyle( 3., DRAW_COLOR);
				mDisp.graphics.lineTo(mRect.x, mRect.y + mRect.height);
				mDisp.graphics.lineTo(mRect.x + mRect.width, mRect.y + mRect.height);
				mDisp.graphics.lineTo(mRect.x + mRect.width, mRect.y );
				mDisp.graphics.lineTo(mRect.x, mRect.y );
				mDisp.graphics.lineTo(mRect.x + mRect.width, mRect.y + mRect.height);*/
				mDisp.graphics.drawRect(mRect.x, mRect.y, mRect.width, mRect.height);
				mDisp.graphics.endFill();
			}
		}
	}

}