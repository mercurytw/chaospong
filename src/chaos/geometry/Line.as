package chaos.geometry 
{
	import flash.geom.Point;
	/**
	 * It's just a line
	 * @author Jeff Cochran
	 */
	public class Line 
	{
		public var A:Point;
		public var B:Point;
		public function Line( ax:Number = 0., ay:Number = 0., bx:Number = 0., by:Number = 0. ) 
		{
			A = new Point(ax, ay);
			B = new Point(bx, by);
		}
		
	}

}