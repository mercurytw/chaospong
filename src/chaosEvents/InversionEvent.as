package chaosEvents 
{
	import chaos.ChaosEvent;
	import chaos.GameManager;
	
	/**
	 * Inverts the screen
	 * @author Jeff Cochran
	 */
	public dynamic class InversionEvent extends ChaosEvent 
	{
		private var mCountdown:Number;
		private var mKeeper:GameManager;
		public function InversionEvent( manager:GameManager ) 
		{
			super();
			difficultyTier = 2;
			eventTime = 10.0;
			mKeeper = manager;
		}
		
		public override function setup():void
		{
			mKeeper.mainSprite.rotation += 180.0 // Keep it relative in case we're stacking.
			mKeeper.mainSprite.x += mKeeper.screenWidth;
			mKeeper.mainSprite.y += mKeeper.screenHeight;
			mCountdown = eventTime;
		}
		
		public override function teardown():void
		{
			mKeeper.mainSprite.rotation -= 180.0
			mKeeper.mainSprite.x -= mKeeper.screenWidth;
			mKeeper.mainSprite.y -= mKeeper.screenHeight;
		}
		
		public override function tick( deltaTime:Number ):void
		{
			if ( (mCountdown -= deltaTime) <= 0.0 )
			{
				mKeeper.chaosMan.enterState( "none" );
			}
		}
	}

}