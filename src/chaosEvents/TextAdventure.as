package chaosEvents 
{
	import chaos.ChaosEvent;
	import chaos.Console;
	import chaos.event.TextInputEvent;
	import chaos.GameManager;
	import chaos.StateMachine;
	import chaosEvents.room.Room;
	import chaosEvents.room.RoomEvent;
	import flash.events.Event;
	import flash.events.EventDispatcher;
	import flash.events.IEventDispatcher;
	import flash.utils.ByteArray;
	
	/**
	 * Drops player into a random text adventure
	 * @author Jeff Cochran
	 */
	public class TextAdventure extends ChaosEvent
	{
		[Embed(source="../assets/adventure.json", mimeType="application/octet-stream")]
		private static const SCRIPT_RAW:Class;
		private var script:String;
		private var keeper:GameManager;
		private var roomSM:StateMachine;
		private var cons:Console;
		public function TextAdventure( manager:GameManager ) 
		{
			super();
			difficultyTier = 1;
			eventTime = -1.0;
			keeper = manager;
			var ba:ByteArray = new SCRIPT_RAW() as ByteArray;
			script = ba.readUTFBytes(ba.length);
			
			cons = new Console( manager );
			parseJSON( script );
		}
		
		public function parseJSON( s:String ):void
		{
			var dict:Object = JSON.parse( s )["roomSM"];
			roomSM = StateMachine.newStateMachineFromObj( dict, Room.newRoomFromObj );
		}
		
		public override function setup():void
		{
			keeper.pauseKeepItUp = true;
			var index:int = int(Math.round(Math.random() * (roomSM.mNames.length - 1)));
			roomSM.enterState(roomSM.mNames[index]);
			Room(roomSM.mActiveState).addEventListener( RoomEvent.DEATH_EVENT, handleRoomEvent );
			Room(roomSM.mActiveState).addEventListener( RoomEvent.LEAVE_EVENT, handleRoomEvent );
			cons.setup();
			cons.clear();
			Room(roomSM.mActiveState).setConsole( cons );
		}
		
		public override function teardown():void
		{
			keeper.pauseKeepItUp = false;
			Room(roomSM.mActiveState).removeEventListener( RoomEvent.DEATH_EVENT, handleRoomEvent );
			Room(roomSM.mActiveState).removeEventListener( RoomEvent.LEAVE_EVENT, handleRoomEvent );
			cons.teardown();
			
		}
		
		public override function tick( deltaTime:Number ):void
		{
			
		}
		
		public function handleRoomEvent(event:RoomEvent):void
		{
			switch( event.type )
			{
				case RoomEvent.DEATH_EVENT:
					BallController.moveSpeed += 40.0;
					keeper.chaosMan.enterState( "none" );
					break;
				case RoomEvent.LEAVE_EVENT:
					keeper.chaosMan.enterState( "none" );
					break;
				default:
					trace("TextAdventure.handleRoomEvent: encountered unknown room event: " + event.type );
					break;
			}
		}
		
		public function toJSON(s:String):*
		{
			return {"roomSM":roomSM };
		}
	}

}