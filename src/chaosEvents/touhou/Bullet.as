package chaosEvents.touhou 
{
	import chaos.Actor;
	import chaos.GameManager;
	import chaos.IPoolable;
	import chaos.ITickable;
	import chaos.Vector2D;
	import flash.display.Bitmap;
	import flash.geom.Point;
	import flash.geom.Rectangle;
	
	/**
	 * A self-controlling square bullet
	 * @author Jeff Cochran
	 */
	public class Bullet extends Actor implements IPoolable, ITickable 
	{
		public static const BULLET_SPEED:Number = 100.0;
		public var direction:Vector2D;
		private var _isAlive:Boolean;
		public function Bullet( drawable:Bitmap, xPosition:Number=0.0, yPosition:Number=0.0, collisionVolume:Rectangle=null) 
		{
			super(drawable, xPosition, yPosition, collisionVolume);
			_isAlive = false;
			direction = new Vector2D();
		}
		
		/* INTERFACE chaos.ITickable */
		
		public function tick(deltaTime:Number):void 
		{
			if ( !_isAlive )
			{
				trace("Bullet.tick: warning: ticking dead bullet!");
				return;
			}
			
			var currPos:Point = getPos();
			currPos.x += direction.x * BULLET_SPEED * deltaTime;
			currPos.y += direction.y * BULLET_SPEED * deltaTime;
			setPos( currPos.x, currPos.y );
			
			return;
		}
		
		/* INTERFACE chaos.IPoolable */
		
		public function isAlive():Boolean 
		{
			return _isAlive;
		}
		
		public function activate():void 
		{
			_isAlive = true;
			mDraw.visible = true;
		}
		
		public function deactivate():void 
		{
			_isAlive = false;
			mDraw.visible = false;
		}
		
	}

}