package chaosEvents.touhou 
{
	import chaos.ActorFactory;
	import chaos.GameManager;
	/**
	 * A pool of bullets for the touhou event
	 * @author Jeff Cochran
	 */
	public class BulletPool 
	{
		private var bullets:Vector.<Bullet>;
		private static const NUM_BULLETS_TO_ALLOC:int = 300.0;
		public function BulletPool( manager:GameManager ) 
		{
			var fact:ActorFactory = new ActorFactory();
			bullets = new Vector.<Bullet>();
			for ( var i:int = 0; i < NUM_BULLETS_TO_ALLOC; i++ )
			{
				var bull:Bullet = fact.buildBullet( manager.resources.getBitmap("ball"), manager, 0.0, 0.0 );
				bullets.push( bull );
			}
		}
		
		public function getBullet():Bullet
		{
			var bull:Bullet = null;
			for ( var i:int = 0; i < bullets.length; i++ )
			{
				if ( !(bullets[i].isAlive()) )
				{
					bullets[i].activate();
					return bullets[i];
				}
			}
			trace("BulletPool.getBullet: WARNING! requested bullet when none were available!");
			trace(">>> length of array is " + bullets.length );
			return null;
		}
		
	}

}