package  chaosEvents
{
	import flash.display.Bitmap;
	import flash.display.Sprite;
	import flash.display3D.textures.Texture;
	import flash.events.KeyboardEvent;
	import flash.events.MouseEvent;
	import flash.text.TextField;
	import flash.text.TextFormat;
	
	import chaos.Actor;
	import chaos.ChaosEvent;
	import chaos.GameManager;
	
	/**
	 * The player must complete a math problem to resume control of the game
	 * @author Jeff Cochran
	 */
	public class MathEvent extends ChaosEvent 
	{
		private var mAnswer:int; 
		private var mKeeper:GameManager;
		private var evtSprite:Sprite = new Sprite();
		private var questText:TextField;
		private var quest:String;
		private var buffer:String = "";
		private var bkBallSpeed:Number;
		
		private const MAX_ANSWER:int = 25.0;
		private const KC_ZERO:uint = 48;
		private const KC_NINE:uint = 57;
		private const KC_ENTER:uint = 13;
		private const KC_BKSP:uint = 8;
		public function MathEvent( manager:GameManager ) 
		{
			super();
			difficultyTier = 3;
			eventTime = -1.0;
			mKeeper = manager;
			initText();
		}
		
		private function initQuestion():void
		{
			var b:int;
			var isAddition:Boolean = ( Math.random() > 0.5 ) ? true : false;
			var other:int = int(Math.floor( Math.random() * MAX_ANSWER ));
			mAnswer = int(Math.floor( Math.random() * MAX_ANSWER ));
			
			if ( other > mAnswer )
			{
				var tmp:int = mAnswer;
				mAnswer = other;
				other = tmp;
			}
			
			if ( isAddition )
			{
				b = mAnswer - other;
				quest = "" + other + " + " + b + " = ";
			}
			else
			{
				b = mAnswer + other;
				quest = "" + b + " - " + other + " = ";
			}
			
			questText.text = quest + "?  ";
			if ( CONFIG::debug )
			{
				trace("MathEvent.initQuestion: answer is " + mAnswer );
			}
		}
		
		private function initText():void
		{
			questText = new TextField();
			var fmt:TextFormat = questText.getTextFormat();
			
			fmt.size = mKeeper.screenHeight / 10.;
			fmt.color = 0xFFFFFF;
			fmt.align = "center";
			questText.defaultTextFormat = fmt;
			questText.text = "ff";
			questText.height = questText.textHeight;
			questText.width = mKeeper.screenWidth;
			questText.y = mKeeper.screenHeight / 2.;
			
			evtSprite.addChild(questText);
		}
		
		public override function setup():void
		{
			buffer = "";
			initQuestion();
			Sprite(mKeeper.mainSprite.getChildAt(GameManager.COVER)).addChild(evtSprite);
			mKeeper.shouldSuspendControl = true;
			
			mKeeper.mainSprite.stage.addEventListener( KeyboardEvent.KEY_DOWN, registerKeyEvent );
			bkBallSpeed = BallController.moveSpeed;
			BallController.moveSpeed = 150.0;;
		}
		
		public override function teardown():void
		{
			Sprite(mKeeper.mainSprite.getChildAt(GameManager.COVER)).removeChild(evtSprite);
			mKeeper.shouldSuspendControl = false;
			
			mKeeper.mainSprite.stage.removeEventListener( KeyboardEvent.KEY_DOWN, registerKeyEvent );
			BallController.moveSpeed = bkBallSpeed;
		}
		
		// compare ans against the value of mAnswer
		private function check( ans:String ):Boolean
		{
			var number:int = 0;
			
			while ( ans.length > 0 )
			{
				number *= 10;
				number += ans.charCodeAt( 0 ) - KC_ZERO;
				ans = ans.substring(1);
			}
			return( number == mAnswer );
		}
		
		
		public function registerKeyEvent( keyEvent:KeyboardEvent ):void
		{
			if ( ( KC_ZERO <= keyEvent.keyCode ) && ( keyEvent.keyCode <= KC_NINE ) )
			{
				buffer += String.fromCharCode(keyEvent.keyCode).toLowerCase();
			}
			else if ( keyEvent.keyCode == KC_BKSP )
			{
				buffer = buffer.substring( 0, buffer.length - 1 );
			}
			questText.text = quest + buffer;
			if (check(buffer))
			{
				mKeeper.chaosMan.enterState( "none" );
			}
		}
		
		public override function tick( deltaTime:Number ):void
		{
			
		}
	}

}