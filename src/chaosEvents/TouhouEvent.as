package chaosEvents 
{
	import chaos.Actor;
	import chaos.ChaosEvent;
	import chaos.GameManager;
	import chaos.ResourceManager;
	import chaos.Vector2D;
	import chaosEvents.touhou.Bullet;
	import chaosEvents.touhou.BulletPool;
	import flash.display.Sprite;
	import flash.text.AntiAliasType;
	import flash.text.TextField;
	import flash.text.TextFormat;
	import flash.text.TextFormatAlign;
	
	/**
	 * Creates and manages bullet hell style game
	 * @author Jeff Cochran
	 */
	public class TouhouEvent extends ChaosEvent 
	{
		private var mCountdown:Number;
		private var keeper:GameManager;
		private var activeBullets:Vector.<Bullet>;
		private var minWidth:Number;
		private var dropTime:Number;
		public static var horizontalBufferSpace:Number = 15.0;
		public static var verticalBufferSpace:Number = 10.0;
		private var lineCountdown:Number;
		private var pool:BulletPool;
		private var ballSpeed:Number;
		private var tutText:TextField;
		private const textDisplayTime:Number = 3.0;
		private var textCountdown:Number;
		private var showText:Boolean = true;
		public function TouhouEvent( manager:GameManager ) 
		{
			super();
			difficultyTier = 4;
			eventTime = 30.0;
			keeper = manager;
			activeBullets = new Vector.<Bullet>();
			pool = new BulletPool( manager );
			
			dropTime = ( manager.resources.getBitmap( "ball" ).width + Actor(manager.objTable.lookup( manager.paddleHandle )).mDraw.width +
						 verticalBufferSpace ) / Bullet.BULLET_SPEED;
			minWidth = ( Actor(manager.objTable.lookup( manager.paddleHandle )).mDraw.width ) + horizontalBufferSpace;
			
			textCountdown = textDisplayTime;
			
			initText();
		}
		
		public function initText():void
		{
			tutText = new TextField();
			tutText.embedFonts = true;
			tutText.antiAliasType = AntiAliasType.ADVANCED;
			tutText.selectable = false;
			tutText.defaultTextFormat = new TextFormat(ResourceManager.METROPHOBIC, keeper.screenHeight / 20., 0xFFFFFF, null, null, null, null, null, TextFormatAlign.CENTER );
			tutText.text = "Dodgeball!";
			tutText.width = tutText.textWidth + 5;
			tutText.height = tutText.textHeight + 5;
			tutText.x = (keeper.screenWidth / 2.) - (tutText.textWidth / 2.);
			tutText.y = (keeper.screenHeight / 2.0) - (tutText.textHeight / 2.);
		}
		
		public override function setup():void
		{
			mCountdown = eventTime;
			lineCountdown = 0.0;
			ballSpeed = BallController.moveSpeed;
			BallController.moveSpeed = 0.0;
			if ( showText )
			{
				Sprite(keeper.mainSprite.getChildAt( GameManager.COVER )).addChild( tutText );
			}
			
		}
		
		public override function teardown():void
		{
			for each ( var b:Bullet in activeBullets )
			{
				keeper.colliders.splice( keeper.colliders.indexOf( b.mVolume ), 1 );
				b.deactivate();
			}
			activeBullets.splice( 0, activeBullets.length );
			BallController.moveSpeed = ballSpeed;
		}
		
		// inclusive
		private function getNumberRange( low:Number, high:Number ):Number
		{
			var res:Number;
			if ( low > high )
			{
				trace( "TouhouEvent.getNumberRange: invalid parameters " );
				return( NaN );
			}
			res = (Math.random() * ( high - low )) + low;
			return res;
		}
		
		private function dropLine():void
		{
			var b:Bullet;
			var pos:Number = 0.0;
			var maxPos:Number = keeper.screenWidth - ( keeper.resources.getBitmap( "ball" ).width / 2.0 );
			while ( ( pos = getNumberRange( pos, pos + 50.0 ) ) <= maxPos )
			{
				b = pool.getBullet();
				keeper.colliders.push( b.mVolume );
				b.setPos( pos, 0.0 );
				b.direction =  new Vector2D( 0.0, 1.0 );
				activeBullets.push( b );
				pos += minWidth;
			}
		}
		
		public override function tick( deltaTime:Number ):void
		{
			if ( ( mCountdown -= deltaTime ) <= 0.0 )
			{
				keeper.chaosMan.enterState( "none" );
				return;
			}
			
			if ( showText )
			{
				if ( ( textCountdown -= deltaTime ) <= 0.0 )
				{
					showText = false;
					tutText.parent.removeChild( tutText );
				}
			}
			
			
			for ( var i:int = 0; i < activeBullets.length; i++ )
			{
				activeBullets[i].tick( deltaTime );
				if ( activeBullets[i].mVolume.intersects( Actor( keeper.objTable.lookup( keeper.paddleHandle )).mVolume ) )
				{
					keeper.isGameOver = true;
				}
			}
			
			var j:int = 0;
			while( j < activeBullets.length )
			{
				if ( activeBullets[j].getPos().y >= ( keeper.screenHeight + ( keeper.resources.getBitmap( "ball" ).height / 2.0 ) ) )
				{
					activeBullets[ j ].deactivate();
					keeper.colliders.splice( keeper.colliders.indexOf( activeBullets[ j ].mVolume ), 1 );
					activeBullets.splice( j, 1 );
				}
				else
				{
					j++;
				}
			}
			
			if ( ( lineCountdown -= deltaTime ) <= 0.0 )
			{
				dropLine();
				lineCountdown = dropTime;
			}
		}
	}

}