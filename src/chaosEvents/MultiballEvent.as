package  chaosEvents
{
	import chaos.ActorFactory;
	import flash.display.Bitmap;
	import flash.display.Sprite;
	import flash.geom.Point;
	import flash.text.TextField;
	import flash.text.TextFormat;
	
	import chaos.Actor;
	import chaos.ChaosEvent;
	import chaos.GameManager;
	
	/**
	 * Puts a ton of balls in the play area
	 * @author Jeff Cochran
	 */
	public class MultiballEvent extends ChaosEvent 
	{
		private static const NUM_BALLS_TO_CREATE:int = 30;
		private var ctlrs:Vector.<BallController>;
		private var mKeeper:GameManager;
		private var mCircleSprite:Sprite = new Sprite();
		private const BLINK_TIME:Number = 0.5;
		private const TUT_TIME:Number = 5.0;
		private var mTutText:TextField;
		private var mScreenCenter:Point;
		public function MultiballEvent( manager:GameManager ) 
		{
			super();
			difficultyTier = 0;
			eventTime = 20.0;
			mKeeper = manager;
			Sprite(mKeeper.mainSprite.getChildAt( GameManager.COVER )).addChild( mCircleSprite );
			ctlrs = new Vector.<BallController>();
			mCircleSprite.visible = false;
			mScreenCenter = new Point( mKeeper.playArea.x + (mKeeper.playArea.width / 2.),
										 mKeeper.playArea.y + (mKeeper.playArea.height / 2.));
			initText();
			var x:Number;
			var y:Number;
			var fact:ActorFactory = new ActorFactory();
			for (var i:int = 0; i < MultiballEvent.NUM_BALLS_TO_CREATE; i++)
			{
				var ball:Bitmap = manager.resources.getBitmap( "greyBall" );
				x = (Math.random() * (manager.playArea.right - (ball.width) - GameManager.MARGIN_SIZE)) + (ball.width / 2.) + GameManager.MARGIN_SIZE;
				y = (Math.random() * (manager.playArea.bottom - ball.height)) + (ball.height / 2.);
				var ballAct:Actor = fact.buildActor( ball, manager, x, y, GameManager.BACKGROUND);
				var ctl:BallController = new BallController( manager, manager.objTable, ballAct, manager.paddleHandle, false  );
				this.ctlrs.push( ctl );
			}
			teardown();
		}
		
		public function drawCircle():void
		{	
			var bact:Actor =  Actor(mKeeper.objTable.lookup( mKeeper.ballHandle ));
			mCircleSprite.graphics.clear();
			mCircleSprite.graphics.beginFill( 0x000000, 0.0 );
			mCircleSprite.graphics.lineStyle( 0, 0x000000 );
			mCircleSprite.graphics.drawCircle( bact.getPos().x, bact.getPos().y, 20.0 );
			mCircleSprite.graphics.endFill();
			
			
		}
		
		private function initText():void
		{
			mTutText = new TextField();
			var fmt:TextFormat = mTutText.getTextFormat();
			
			fmt.size = mKeeper.screenHeight / 10.;
			fmt.color = 0xFF0000;
			fmt.align = "center";
			mTutText.defaultTextFormat = fmt;
			mTutText.text = "EYES ON THE BALL!";
			mTutText.height = mTutText.textHeight;
			mTutText.width = mKeeper.screenWidth;
			mTutText.x = mScreenCenter.x - (mTutText.width / 2.);
			mTutText.y = mScreenCenter.y - 10.0;
			//mTutText.y = mKeeper.screenHeight / 2.;
			
			
			mCircleSprite.addChild(mTutText);
		}
		
		private function activate( act:Actor ):void
		{
			act.mVolume.setDrawTarget( Sprite(mKeeper.mainSprite.getChildAt( GameManager.COVER )) );
			act.mDraw.visible = true;
			mKeeper.colliders.push( act.mVolume );
			mKeeper.updateList.push( act.mVolume );
		}
		
		private function deactivate( act:Actor ):void
		{
			act.mVolume.removeDrawTarget();
			act.setPos( act.mInitialPosition.x, act.mInitialPosition.y );
			act.mDraw.visible = false;
			mKeeper.colliders.slice( mKeeper.colliders.indexOf( act.mVolume ), 1 );
			mKeeper.updateList.slice( mKeeper.updateList.indexOf( act.mVolume ), 1 );
		}
		
		public override function setup():void
		{
			mCircleSprite.visible = true;
			for each ( var ctlr:BallController in ctlrs )
			{
				activate(ctlr.mPawn);
				mKeeper.updateList.push( ctlr );
				ctlr.mDir.setTo( Math.random() + .1, Math.random() + .1 );
				ctlr.mDir.normalize();
			}
			agg = 0.0;
		}
		
		public override function teardown():void
		{
			mCircleSprite.visible = false;
			for each ( var ctlr:BallController in ctlrs )
			{
				deactivate( ctlr.mPawn );
				mKeeper.updateList.splice( mKeeper.updateList.indexOf( ctlr ), 1 );
			}
		}
		
		private var agg:Number = 0.0;
		private var tutAgg:Number = 0.0;
		private var txtRot:Number = 12.0;
		public override function tick( deltaTime:Number ):void
		{
			mCircleSprite.graphics.clear();
			mTutText.visible = false;
			agg += deltaTime;
			if ( tutAgg <= TUT_TIME )
			{
				tutAgg = agg;
				
				
				if ( ( int( agg / BLINK_TIME ) % 2 ) == 1 )
				{
					mTutText.visible = true;
					//mTutText.rotation = (txtRot *= -1.0);
					drawCircle();
				}
			}
			if ( agg >= this.eventTime )
			{
				mKeeper.chaosMan.enterState( "none" );
			}
		}
	}

}