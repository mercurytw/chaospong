package chaosEvents 
{
	import chaos.ChaosEvent;
	import chaos.GameManager;
	import chaos.ResourceManager;
	import flash.display.Sprite;
	import flash.text.AntiAliasType;
	import flash.text.TextField;
	import flash.text.TextFormat;
	import flash.text.TextFormatAlign;
	import flash.ui.Mouse;
	
	/**
	 * Causes the player to need to use they keyboard to control movement
	 * @author Jeff Cochran
	 */
	public class KeyControlEvent extends ChaosEvent 
	{
		private var mCountdown:Number;
		private var mKeeper:GameManager;
		private var tutText:TextField;
		private var warnText:TextField;
		private static const TUT_TIME:Number = 5.0;
		private var timerAgg:Number = 0.0;
		private var isTutTextShowing:Boolean = true;
		private var isWarnTextShowing:Boolean = false;
		public function KeyControlEvent( manager:GameManager ) 
		{
			super();
			
			difficultyTier = 0;
			eventTime = 30.0;
			mKeeper = manager;
			
			initText();
		}
		
		public function initText():void
		{
			tutText = new TextField();
			tutText.embedFonts = true;
			tutText.antiAliasType = AntiAliasType.ADVANCED;
			tutText.selectable = false;
			tutText.defaultTextFormat = new TextFormat(ResourceManager.KITCHEN_POLICE, mKeeper.screenHeight / 20., 0xFFFFFF, null, null, null, null, null, TextFormatAlign.CENTER );
			tutText.text = "use the arrow keys!";
			tutText.width = tutText.textWidth + 5;
			tutText.height = tutText.textHeight + 5;
			tutText.x = (mKeeper.screenWidth / 2.) - (tutText.textWidth / 2.);
			tutText.y = (mKeeper.screenHeight / 2.0) - (tutText.textHeight / 2.);
			
			warnText = new TextField();
			warnText.embedFonts = true;
			warnText.antiAliasType = AntiAliasType.ADVANCED;
			warnText.selectable = false;
			warnText.defaultTextFormat = new TextFormat(ResourceManager.KITCHEN_POLICE, mKeeper.screenHeight / 20., 0xFFFFFF, null, null, null, null, null, TextFormatAlign.CENTER );
			warnText.text = "use the mouse in 3...";
			warnText.width = warnText.textWidth + 5;
			warnText.height = warnText.textHeight + 5;
			warnText.x = (mKeeper.screenWidth / 2.) - (warnText.textWidth / 2.);
			warnText.y = (mKeeper.screenHeight / 2.0) - (warnText.textHeight / 2.);
			Sprite(mKeeper.mainSprite.getChildAt( GameManager.COVER )).addChild( warnText );
			warnText.visible = false;
		}
		
		public override function setup():void
		{
			mCountdown = eventTime;
			timerAgg = TUT_TIME;
			isTutTextShowing = true;
			isWarnTextShowing = false;
			mKeeper.paddleState.enterState( "keyboard" );
			Sprite(mKeeper.mainSprite.getChildAt( GameManager.COVER )).addChild( tutText );
			Mouse.show();
			warnText.visible = false;
		}
		
		public override function teardown():void
		{
			mKeeper.paddleState.enterState( "normal" );
			if ( tutText.parent != null )
			{
				tutText.parent.removeChild( tutText );
			}
			if ( warnText.visible )
			{
				warnText.visible = false;
			}
			Mouse.hide();
		}
		
		public override function tick( deltaTime:Number ):void
		{
			if ( isTutTextShowing )
			{
				if ( ( timerAgg -= deltaTime ) <= 0.0 )
				{
					tutText.parent.removeChild( tutText );
					isTutTextShowing = false;
				}
			}
			if ( !isWarnTextShowing )
			{
				if ( mCountdown <= 3.0 )
				{
					warnText.visible = true;
					isWarnTextShowing = true;
				}
			}
			else
			{
				warnText.text = "use the mouse in " + Math.round(mCountdown ).toString() + "...";
			}
			if ( ( mCountdown -= deltaTime ) <= 0.0 )
			{
				mKeeper.chaosMan.enterState( "none" );
			}
		}
		
	}

}