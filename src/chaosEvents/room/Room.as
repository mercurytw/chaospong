package chaosEvents.room 
{
	import chaos.Console;
	import chaos.event.TextInputEvent;
	import chaos.IState;
	import flash.events.EventDispatcher;
	import flash.utils.Dictionary;
	/**
	 * Represents a single room in a text adventure
	 * @author Jeff Cochran
	 */
	public class Room extends EventDispatcher implements IState
	{
		private var choices:Vector.<Choice>;
		public var description:String;
		private var stateFlags:Dictionary;
		private var cons:Console;
		private var extendedDescription:String;
		private var exiting:Boolean;
		private var exitFunc:Function;
		public function Room( description:String, choices:Vector.<Choice> ) 
		{
			super();
			this.description = description;
			this.choices = choices;
			stateFlags = new Dictionary();
		}
		
		public static function newRoomFromObj(obj:Object):Room
		{
			var r:Room;
			var v:Vector.<Choice> = new Vector.<Choice>();
			
			for each ( var o:Object in obj["choices"] )
			{
				v.push( Choice.newChoiceFromObj( o ) );
			}
			r = new Room( obj["description"], v );
			
			return( r );
		}
		
		public function setup():void
		{
			extendedDescription = "";
			exiting = false;
		}
		
		public function teardown():void
		{
			cons.removeEventListener( TextInputEvent.TEXT_INPUT, handleTextInputEvent );
			stateFlags = new Dictionary();
		}
		
		public function setConsole( c:Console ):void
		{
			cons = c;
			cons.addEventListener( TextInputEvent.TEXT_INPUT, handleTextInputEvent );
			cons.write( "\n ** type \"look\" at any time to redisplay the description of the room ** \n" );
			
			describe();
		}
		
		private function describe():void
		{
			cons.write( description + " " + extendedDescription );
			
			for each( var c:Choice in choices )
			{
				if ( isChoiceValid( c ) )
				{
					cons.write("\n> " + c.command );
				}
			}
		}
		
		public function handleTextInputEvent( event:TextInputEvent ):void
		{
			if ( exiting )
			{
				exitFunc();
			}
			if ( event.message == "look" )
			{
				cons.clear();
				describe();
				return;
			}
			for each( var c:Choice in choices )
			{
				if ( c.command.toLowerCase() == event.message.toLowerCase() )
				{
					if ( isChoiceValid( c ) )
					{
						performChoice( c );
						cons.clear();
						cons.write( "\n" + c.description + "\n" );
						if ( !exiting )
						{
							describe();
						} 
						else
						{
							cons.write( " press enter to continue... " );
						}
						return;
					}
					else
					{
						break;
					}
				}
			}
			cons.write("\nThat isn't an option");
		}
		
		private function isChoiceValid( c:Choice ):Boolean
		{
			for each( var r:Requirement in c.reqs )
			{
				if ( ( stateFlags[r.flag] == null ) && ( r.truth == true ) )
				{
					return false;
				}
				if ( (stateFlags[r.flag] != null) && ( stateFlags[r.flag] != r.truth ) )
				{
					return false;
				}
			}
			return true;
		}
		
		
		public function setState(flag:String, shouldSet:Boolean):void
		{
			stateFlags[flag] = shouldSet;
		}
		
		private function performChoice( c:Choice ):void
		{
			for each( var e:Effect in c.effects )
			{
				switch( e.type )
				{
					case Effect.EXTEND_DESCR:
						extendedDescription += e.data;
						break;
					case Effect.DIE:
						exiting = true;
						exitFunc = function():void {
							dispatchEvent( new RoomEvent( RoomEvent.DEATH_EVENT ) );
						}
						break;
					case Effect.LEAVE:
						exiting = true;
						exitFunc = function():void {
							dispatchEvent( new RoomEvent( RoomEvent.LEAVE_EVENT ) );
						}
						break;
					case Effect.SET_FLAG:
						setState( e.data, true );
						break;
					case Effect.UNSET_FLAG:
						setState( e.data, false );
						break;
					default:
						trace("Room.performChoice: got invalid effect: " + e.type );
						break;
				}
			}
		}
		
		public function toJSON(s:String):*
		{
			return {"description":description,"choices":choices };
		}
		
		
	}

}