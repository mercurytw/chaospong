package chaosEvents.room 
{
	import flash.events.Event;
	
	/**
	 * An event that can happen in a text adventure
	 * @author Jeff Cochran
	 */
	public class RoomEvent extends Event 
	{
		public static const LEAVE_EVENT:String = "leaveEvent";
		public static const DEATH_EVENT:String = "deathEvent";
	
		public function RoomEvent(type:String, bubbles:Boolean=false, cancelable:Boolean=false) 
		{ 
			super(type, bubbles, cancelable);
			
		} 
		
		public override function clone():Event 
		{ 
			return new RoomEvent(type, bubbles, cancelable);
		} 
		
		public override function toString():String 
		{ 
			return formatToString("RoomEvent", "type", "bubbles", "cancelable", "eventPhase"); 
		}
		
	}
	
}