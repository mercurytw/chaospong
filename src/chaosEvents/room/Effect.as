package chaosEvents.room 
{
	/**
	 * Effects a player's choice can have
	 * @author Jeff Cochran
	 */
	public class Effect 
	{
		public static const SET_FLAG:String = "setFlag";
		public static const UNSET_FLAG:String = "unsetFlag";
		public static const EXTEND_DESCR:String = "extend";
		public static const LEAVE:String = "leave";
		public static const DIE:String = "die";
		
		public var type:String;
		public var data:String;
		public function Effect( type:String, data:String=null ) 
		{
			this.type = type;
			this.data = data;
		}
		
		public static function newEffectFromObj(obj:Object):Effect
		{
			var e:Effect = new Effect( obj["type"], obj["data"] );
			return e;
		}
		
	}

}