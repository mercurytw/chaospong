package chaosEvents.room 
{
	/**
	 * A requirement class
	 * @author Jeff Cochran
	 */
	public class Requirement 
	{
		public static const SET:Boolean = true;
		public static const UNSET:Boolean = false;
		
		public var flag:String;
		public var truth:Boolean;
		public function Requirement( flag:String, truth:Boolean ) 
		{
			this.flag = flag;
			this.truth = truth;
		}
		
		public static function newRequirementFromObj(obj:Object):Requirement
		{
			var r:Requirement = new Requirement( obj["flag"], obj["truth"] );
			return r;
		}
		
	}

}