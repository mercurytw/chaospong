package chaosEvents.room 
{
	/**
	 * A choice a player can make in a text adventure
	 * @author Jeff Cochran
	 */
	public class Choice 
	{
		public var reqs:Vector.<Requirement>;
		public var command:String;
		public var description:String;
		public var effects:Vector.<Effect>; // applied in order
		public function Choice() 
		{
			
		}
		
		public static function newChoiceFromObj(obj:Object):Choice
		{
			var c:Choice = new Choice();
			var reqs:Vector.<Requirement> = new Vector.<Requirement>();
			var effects:Vector.<Effect> = new Vector.<Effect>();
			c.command = obj["command"];
			c.description = obj["description"]; 
			
			for each ( var o:Object in obj["reqs"] )
			{
				reqs.push( Requirement.newRequirementFromObj( o ) );
			}
			c.reqs = reqs;
			
			for each ( var o2:Object in obj["effects"] )
			{
				effects.push( Effect.newEffectFromObj( o2 ) );
			}
			c.effects = effects;
			
			return( c );
		}
	}

}