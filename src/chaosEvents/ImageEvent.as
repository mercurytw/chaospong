package chaosEvents 
{
	import chaos.ChaosEvent;
	import chaos.GameManager;
	import flash.display.Bitmap;
	import flash.display.Sprite;
	import flash.geom.Point;
	
	/**
	 * Flashes obnoxious images in the background
	 * @author Jeff Cochran
	 **/
	public class ImageEvent extends ChaosEvent 
	{
		private var mCountdown:Number;
		private var mKeeper:GameManager;
		private const MIN_SHOW_TIME:Number = 0.5;
		private const MAX_SHOW_TIME:Number = 2.0;
		private var mShowTime:Number = 0.0;
		private var mCurrentSprite:Bitmap;
		private var mNames:Array = new Array( "face", "face2", "face3", "cowboy", "worm" );
		public function ImageEvent( manager:GameManager ) 
		{
			super();
			difficultyTier = 0;
			eventTime = 20.0;
			mKeeper = manager;
		}
		
		public override function setup():void
		{
			mCountdown = eventTime;
			mShowTime = -1.0;
			mCurrentSprite = null;
		}
		
		public override function teardown():void
		{
			mKeeper.paddleState.enterState( "normal" );
			mCurrentSprite.parent.removeChild( mCurrentSprite );
		}
		
		// inclusive
		private function getNumberRange( low:Number, high:Number ):Number
		{
			var res:Number;
			if ( low > high )
			{
				trace( "ImageEvent.getNumberRange: invalid parameters " );
				return( NaN );
			}
			res = (Math.random() * ( high - low )) + low;
			return res;
		}
		
		private function showRandomImage():void
		{
			var x:Number;
			var y:Number;
			if ( mCurrentSprite != null )
			{
				mCurrentSprite.parent.removeChild( mCurrentSprite );
			}
			
			var index:int = int(Math.round( getNumberRange( 0.0, mNames.length - 1.0 ) ));
			mCurrentSprite = mKeeper.resources.getBitmap( mNames[index] );
			x = getNumberRange( 0.0, mKeeper.screenWidth - mCurrentSprite.width );
			y = getNumberRange( 0.0, mKeeper.screenHeight - mCurrentSprite.height );
			Sprite(mKeeper.mainSprite.getChildAt( GameManager.BACKGROUND )).addChild( mCurrentSprite );
			mCurrentSprite.x = x;
			mCurrentSprite.y = y;
		}
		
		public override function tick( deltaTime:Number ):void
		{
			if ( ( mShowTime -= deltaTime ) <= 0.0 )
			{
				showRandomImage();
				mShowTime = MIN_SHOW_TIME;//getNumberRange( MIN_SHOW_TIME, MAX_SHOW_TIME );
			}
			if ( ( mCountdown -= deltaTime ) <= 0.0 )
			{
				mKeeper.chaosMan.enterState( "none" );
			}
		}
	}
}