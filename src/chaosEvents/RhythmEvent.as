package chaosEvents 
{
	import adobe.utils.CustomActions;
	import chaos.ChaosEvent;
	import chaos.GameManager;
	import chaos.ResourceManager;
	import flash.display.Bitmap;
	import flash.display.Sprite;
	import flash.events.KeyboardEvent;
	import flash.text.AntiAliasType;
	import flash.text.TextField;
	import flash.text.TextFormat;
	import flash.text.TextFormatAlign;
	import flash.ui.Keyboard;
	/**
	 * A quick rhythm event game. Pauses gameplay
	 * @author Jeff Cochran
	 */
	public class RhythmEvent extends ChaosEvent
	{
		private const BG_TEXT_MAX_SCALE:Number = 3.0;
		private const NUM_OF_CHALLENGES:int = 5;
		private const TIME_MAX:Number = 3.0;
		private const TIME_MIN:Number = 2.0;
		private const READY_TIME:Number = 3.0;
		private var motions:Array;
		private var countdown:Number;
		private var first:Boolean = true;
		private var keeper:GameManager;
		private var readyText:TextField;
		private var bgReadyText:TextField;
		private var pressText:TextField;
		private var isShowingTutText:Boolean;
		private var alpha:Number;
		private var currentBehavior:Function;
		private var currentDirection:Object;
		private var cleanFunc:Function;
		private var challengesPassed:int;
		private var guy:Bitmap;
		private static var shouldEnd:Boolean;
		public function RhythmEvent(manager:GameManager) 
		{
			super();
			difficultyTier = 1;
			eventTime = -1.0;
			keeper = manager;
			
			motions = new Array();
			var obj:Object = new Object();
			obj.arrow = manager.resources.getBitmap("up");
			obj.code = Keyboard.UP;
			obj.guy = manager.resources.getBitmap("guyUp");
			motions.push(obj);
			
			obj = new Object();
			obj.arrow = manager.resources.getBitmap("right");
			obj.code = Keyboard.RIGHT;
			obj.guy = manager.resources.getBitmap("guyRight");
			motions.push(obj);
			
			obj = new Object();
			obj.arrow = manager.resources.getBitmap("down");
			obj.code = Keyboard.DOWN;
			obj.guy = manager.resources.getBitmap("guyDown");
			motions.push(obj);
			
			obj = new Object();
			obj.arrow = manager.resources.getBitmap("left");
			obj.code = Keyboard.LEFT;
			obj.guy = manager.resources.getBitmap("guyLeft");
			motions.push(obj);
			
			initText();
		}
		
		public function initText():void
		{
			readyText = new TextField();
			readyText.embedFonts = true;
			readyText.antiAliasType = AntiAliasType.ADVANCED;
			readyText.selectable = false;
			readyText.defaultTextFormat = new TextFormat(ResourceManager.METROPHOBIC, keeper.screenHeight / 20., 0xFFFFFF, null, null, null, null, null, TextFormatAlign.CENTER );
			readyText.text = "READY?";
			readyText.width = readyText.textWidth + 5;
			readyText.height = readyText.textHeight + 5;
			readyText.x = (keeper.screenWidth / 2.) - (readyText.textWidth / 2.);
			readyText.y = (keeper.screenHeight / 2.0) - (readyText.textHeight / 2.);
			
			bgReadyText = new TextField();
			bgReadyText.embedFonts = true;
			bgReadyText.antiAliasType = AntiAliasType.ADVANCED;
			bgReadyText.selectable = false;
			bgReadyText.defaultTextFormat = new TextFormat(ResourceManager.METROPHOBIC, keeper.screenHeight / 20., 0xFFFFFF, null, null, null, null, null, TextFormatAlign.CENTER );
			bgReadyText.text = "READY?";
			bgReadyText.width = bgReadyText.textWidth + 5;
			bgReadyText.height = bgReadyText.textHeight + 5;
			bgReadyText.x = (keeper.screenWidth / 2.) - (bgReadyText.textWidth / 2.);
			bgReadyText.y = (keeper.screenHeight / 2.0) - (bgReadyText.textHeight / 2.);
			
			pressText = new TextField();
			pressText.embedFonts = true;
			pressText.antiAliasType = AntiAliasType.ADVANCED;
			pressText.selectable = false;
			pressText.defaultTextFormat = new TextFormat(ResourceManager.METROPHOBIC, keeper.screenHeight / 20., 0xFFFFFF, null, null, null, null, null, TextFormatAlign.CENTER );
			pressText.text = "PRESS";
			pressText.width = pressText.textWidth + 5;
			pressText.height = pressText.textHeight + 5;
			pressText.x = (keeper.screenWidth / 2.) - (pressText.textWidth / 2.);
			pressText.y = (keeper.screenHeight / 2.0) - (pressText.textHeight / 2.);
		}
		
		public override function setup():void
		{
			first = true;
			isShowingTutText = true;
			bgReadyText.scaleX = 1.0;
			bgReadyText.scaleY = 1.0;
			bgReadyText.alpha = 1.0;
			keeper.pauseKeepItUp = true;
			challengesPassed = 0;
			guy = null;
			RhythmEvent.shouldEnd = false;
		}
		
		public override function teardown():void
		{
			keeper.pauseKeepItUp = false;
			cleanFunc.call();
		}
		
		private function ready():void
		{
			countdown = READY_TIME;
			Sprite(keeper.mainSprite.getChildAt( GameManager.COVER )).addChild( bgReadyText );
			Sprite(keeper.mainSprite.getChildAt( GameManager.COVER )).addChild( readyText );
			currentBehavior = doReady;
			cleanFunc = function():void 
						{
							isShowingTutText = false;
							Sprite(keeper.mainSprite.getChildAt( GameManager.COVER )).removeChild( bgReadyText );
							Sprite(keeper.mainSprite.getChildAt( GameManager.COVER )).removeChild( readyText );
							cleanFunc = function():void { return; };
						};
		}
		
		private function doReady():void
		{
			var percentComplete:Number = ( READY_TIME - countdown ) / READY_TIME;
			bgReadyText.scaleX += (BG_TEXT_MAX_SCALE - (bgReadyText.scaleX - 1.0)) * .1;
			bgReadyText.scaleY += (BG_TEXT_MAX_SCALE - (bgReadyText.scaleY - 1.0)) * .1;
			bgReadyText.x = (keeper.screenWidth / 2.) - ((bgReadyText.textWidth * bgReadyText.scaleX) / 2.);
			bgReadyText.y = (keeper.screenHeight / 2.0) - ((bgReadyText.textHeight * bgReadyText.scaleY) / 2.);
			alpha = bgReadyText.alpha;
			bgReadyText.alpha = (( alpha += ( 0.0 - bgReadyText.alpha ) * .1) >= 0.0 ) ? alpha : 0.0;
		}
		
		// inclusive
		private function getNumberRange( low:Number, high:Number ):Number
		{
			var res:Number;
			if ( low > high )
			{
				trace( "RhythmEvent.getNumberRange: invalid parameters " );
				return( NaN );
			}
			res = (Math.random() * ( high - low )) + low;
			return res;
		}
		
		private function setupNewDirection():void
		{
			var combinedWidth:Number;
			var shouldBeEasy:Boolean = isShowingTutText; // is this our first challenge?
			cleanFunc.call();
			if ( guy == null )
			{
				guy = keeper.resources.getBitmap("guyNeutral");
			}
			else
			{
				guy = currentDirection.guy;
			}
			currentDirection = motions[ int(Math.round(getNumberRange( 0, motions.length - 1))) ];
			combinedWidth = Bitmap(currentDirection.arrow).width + pressText.textWidth + 5;
			pressText.x = (keeper.screenWidth / 2.) - ( combinedWidth / 2.);
			pressText.y = (keeper.screenHeight / 2.) - ( pressText.height / 2. );
			
			Sprite(keeper.mainSprite.getChildAt( GameManager.COVER )).addChild( pressText );
			Bitmap(currentDirection.arrow).x = (keeper.screenWidth / 2.);
			Bitmap(currentDirection.arrow).y = (keeper.screenHeight / 2.) - ( Bitmap(currentDirection.arrow).height / 2. );
			Sprite(keeper.mainSprite.getChildAt( GameManager.COVER )).addChild( currentDirection.arrow );
			
			
			guy.x = (keeper.screenWidth / 2.) - ( guy.width / 2.);
			guy.y = (keeper.screenHeight / 2.) - ( ( Bitmap(currentDirection.arrow).height + guy.height ) / 2.);
			Sprite(keeper.mainSprite.getChildAt( GameManager.COVER )).addChild( guy );
			
			countdown = shouldBeEasy ? TIME_MAX : getNumberRange( TIME_MIN, TIME_MAX );
			keeper.mainSprite.stage.addEventListener(KeyboardEvent.KEY_DOWN, listenForDirection);
			
			cleanFunc = function():void
						{
							Sprite(keeper.mainSprite.getChildAt( GameManager.COVER )).removeChild( pressText );
							Sprite(keeper.mainSprite.getChildAt( GameManager.COVER )).removeChild( currentDirection.arrow );
							Sprite(keeper.mainSprite.getChildAt( GameManager.COVER )).removeChild( guy );
							keeper.mainSprite.stage.removeEventListener(KeyboardEvent.KEY_DOWN, listenForDirection);
							cleanFunc = function():void { return; };
							currentBehavior = function():void { return; };
						}
			currentBehavior = animateArrow;
			Bitmap(currentDirection.arrow).scaleX = 2.0;
			Bitmap(currentDirection.arrow).scaleY = 2.0;
		}
		
		private function animateArrow():void
		{
			Bitmap(currentDirection.arrow).scaleX += ( 1.0 - Bitmap(currentDirection.arrow).scaleX ) * .1;
			Bitmap(currentDirection.arrow).scaleY += ( 1.0 - Bitmap(currentDirection.arrow).scaleY ) * .1;
			Bitmap(currentDirection.arrow).x = (keeper.screenWidth / 2.);
			Bitmap(currentDirection.arrow).y = (keeper.screenHeight / 2.) - ( Bitmap(currentDirection.arrow).height / 2. );
		}
		
		public function listenForDirection( keyEvent:KeyboardEvent ):void
		{
			keeper.mainSprite.stage.removeEventListener(KeyboardEvent.KEY_DOWN, listenForDirection);
			if ( keyEvent.keyCode == currentDirection.code )
			{
				if ( ++challengesPassed < NUM_OF_CHALLENGES )
				{
					setupNewDirection();
				}
				else
				{
					keeper.chaosMan.enterState("none");
				}
			}
			else
			{
				RhythmEvent.shouldEnd = true;
			}
		}
		
		public override function tick( deltaTime:Number ):void
		{
			// Not sure why I have to do this here, but doing it in the event callback
			// causes the game to hang
			if ( RhythmEvent.shouldEnd == true ) 
			{
				keeper.isGameOver = true;
				cleanFunc.call();
			}
			
			if ( keeper.isGameOver )
			{
				return;
			}
			
			if ( first == true )
			{
				first = false;
				ready();
				return;
			}
			countdown -= deltaTime;
			if ( countdown > 0.0 )
			{
				currentBehavior.call();
			}
			else
			{
				if ( isShowingTutText )
				{
					setupNewDirection();
					return;
				}
				keeper.isGameOver = true;
				cleanFunc.call();
			}
		}
	}

}