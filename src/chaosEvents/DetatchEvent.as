package chaosEvents 
{
	import chaos.ChaosEvent;
	import chaos.GameManager;
	
	/**
	 * Causes paddle to detatch from bottom of screen
	 * @author Jeff Cochran
	 */
	public class DetatchEvent extends ChaosEvent 
	{
		private var mCountdown:Number;
		private var mKeeper:GameManager;
		public function DetatchEvent( manager:GameManager ) 
		{
			super();
			difficultyTier = 2;
			eventTime = 30.0;
			mKeeper = manager;
		}
		
		public override function setup():void
		{
			mCountdown = eventTime;
			mKeeper.paddleState.enterState( "detatch" );
		}
		
		public override function teardown():void
		{
			mKeeper.paddleState.enterState( "normal" );
		}
		
		public override function tick( deltaTime:Number ):void
		{
			if ( ( mCountdown -= deltaTime ) <= 0.0 )
			{
				mKeeper.chaosMan.enterState( "none" );
			}
		}
		
	}

}