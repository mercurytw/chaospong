package  
{
	import chaos.CollisionVolume;
	import chaos.GameManager;
	import chaos.ChaosManager;
	import chaosEvents.room.Room;
	import chaosEvents.room.RoomEvent;
	import chaosEvents.TouhouEvent;
	import flash.display.Sprite;
	import flash.display3D.textures.Texture;
	import flash.events.Event;
	import flash.text.TextField;
	import flash.text.TextFormat;
	import flash.events.KeyboardEvent;
	
	/**
	 * Standard game debugging console.
	 * @author Jeff Cochran
	 */
	public class DebugConsole 
	{
		private var mKeeper:GameManager;
		private var isActive:Boolean = false; 
		private var windowSprite:Sprite = new Sprite();
		private var windowText:TextField = new TextField();
		private var textFields:Vector.<TextField> = new Vector.<TextField>();
		private var lines:Vector.<String> = new Vector.<String>();
		private const CONSOLE_SIZE:int = 10;
		private const KC_ENTER:uint = 13;
		private const KC_SHIFT:uint = 16;
		private const KC_A:uint = 65;
		private const KC_Z:uint = 90;
		private const KC_ZERO:uint = 48;
		private const KC_NINE:uint = 57;
		private const KC_BKSP:uint = 8;
		private const KC_SP:uint = 32;
		private var isShiftDown:Boolean = false;
		
		public function DebugConsole( manager:GameManager ) 
		{
			mKeeper = manager;
		
			windowSprite.visible = false;
			windowSprite.graphics.beginFill( 0x000000, 0.8 );
			windowSprite.graphics.drawRect( 0., 0., mKeeper.screenWidth, mKeeper.screenHeight );
			windowSprite.graphics.endFill();
			windowSprite.width = mKeeper.screenWidth;
			windowSprite.height = (mKeeper.screenHeight * 3. ) / 4.;
			
			initText();
			mKeeper.mainSprite.addChild( windowSprite );
		}
		
		private function setLines():void
		{
			for ( var i:int = 0; i < CONSOLE_SIZE; i++ )
			{
				textFields[i].text = "";
				
				if ( i == ( CONSOLE_SIZE - 1 ) )
				{
					textFields[i].text += "> ";
				}
				textFields[i].text += lines[i] + " ";
			}
		}
		
		private function initText():void
		{
			var fmt:TextFormat = windowText.getTextFormat();
			fmt.size = (windowSprite.height / (CONSOLE_SIZE * 1.)) - 2.;
			fmt.color = 0xFFFFFF;
			fmt.align = "left";
			
			for ( var i:int = 0; i < CONSOLE_SIZE; i++ )
			{
				var txt:TextField = new TextField();
				txt.wordWrap = false;
				txt.defaultTextFormat = fmt;
				txt.text =  "";
				txt.width = windowSprite.width + 2.;
				txt.y = i * ( fmt.size + 2. );
				windowSprite.addChild( txt );
				textFields.push( txt );
				lines[i] = "";
			}
			
			setLines();
		}
		
		private function perform( cmd:String ):void
		{
			var res:String = "";
			var re:RegExp =  /\s+/;
			var arr:Array = cmd.split(re);
			
			if ( arr[0] == "" ) { arr.shift(); }
			if ( arr[arr.length - 1] == "" ) { arr.pop(); }
			
			switch( arr[0] )
			{
				case "drawcv":
					mKeeper.shouldDrawCollisionVolumes = !mKeeper.shouldDrawCollisionVolumes;
					res += "Collision volumes set to ";
					res += (mKeeper.shouldDrawCollisionVolumes) ? "visible" : "invisible";
					
					for each ( var col:CollisionVolume in mKeeper.colliders )
					{
						col.visible = mKeeper.shouldDrawCollisionVolumes;
					}
					break;
				case "infinite":
					mKeeper.infinitePlay = !mKeeper.infinitePlay;
					res += "Infinite play ";
					res += (mKeeper.infinitePlay) ? "enabled" : "disabled";
					break;
				case "invoke":
					if ( arr.length != 2 )
					{
						res = "\"cause\" takes exactly 1 parameter";
					}
					else
					{
						ChaosManager(mKeeper.chaosMan).invokeEvent( arr[1] );
						res = "Triggering chaos event \"" + arr[1] + "\"";
					}
					break;
				case "resume":
					this.toggleConsoleWindow();
					break;
				case "set":
					if ( arr[1] == "speed" )
					{
						BallController.moveSpeed = Number(arr[2]);
						res = "Ball speed set to " + arr[2];
					}
					else if ( arr[1] == "drop_handicap" )
					{
						TouhouEvent.verticalBufferSpace = Number( arr[2] );
						res = "Buffer Space set to " + arr[2];
					}
					break;
				case "get":
					if ( arr[1] == "speed" )
					{
						res = "Current ball speed is " + BallController.moveSpeed;
					}
					break;
				default:
					res = "Unknown command: " + cmd;
					break;
			}
			lines.splice(0, 1);
			lines[CONSOLE_SIZE - 2] = res;
			lines[CONSOLE_SIZE - 1] = "";
		}
		
		private var buffer:String = "";
		public function registerKeyEvent( keyEvent:KeyboardEvent ):void
		{
			if ( ( ( KC_A <= keyEvent.keyCode ) && ( keyEvent.keyCode <= KC_Z ) ) ||
				 ( ( KC_ZERO <= keyEvent.keyCode ) && ( keyEvent.keyCode <= KC_NINE ) ) ||
				 ( keyEvent.keyCode == KC_SP ) )
			{
				buffer += String.fromCharCode(keyEvent.keyCode).toLowerCase();
			}
			else if ( keyEvent.keyCode == KC_BKSP )
			{
				buffer = buffer.substring( 0, buffer.length - 1 );
			}
			else if ( keyEvent.keyCode == KC_ENTER )
			{
				perform(buffer);
				buffer = "";
			}
			lines[CONSOLE_SIZE - 1] = buffer.toLowerCase();
			setLines();
		}
		
		/**
		 * Activates or deactivates this console
		 */
		public function toggleConsoleWindow():void
		{
			isActive = windowSprite.visible = mKeeper.pause = !isActive;
			
			if ( isActive ) // we just activated
			{
				buffer = "";
				mKeeper.mainSprite.stage.addEventListener( KeyboardEvent.KEY_DOWN, registerKeyEvent );
			}
			else
			{
				mKeeper.mainSprite.stage.removeEventListener( KeyboardEvent.KEY_DOWN, registerKeyEvent );
			}
		}
		
	}

}