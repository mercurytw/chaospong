package  
{
	import chaos.Actor;
	import chaos.GameManager;
	import chaos.geometry.Line;
	import chaos.HandleTable;
	import chaos.ITickable;
	import chaos.Vector2D;
	import flash.events.Event;
	import flash.events.EventDispatcher;
	import flash.geom.Point;
	import flash.geom.Rectangle;
	
	/**
	 * Controlls the main ball that the player must bounce
	 * @author Jeff Cochran
	 */
	public class BallController implements ITickable
	{
		private var mReducedPlayArea:Rectangle;
		public var keeper:GameManager;
		public var mPawn:Actor;
		public static const INITIAL_MOVE_SPEED:Number = 200.0;
		private const RAMP_RATE:Number = 5.0;
		private const MAX_MOVE_SPEED:Number = 1000.0; // or else collision breaks down
		public static var moveSpeed:Number = 200.0;
		public var mDir:Vector2D = new Vector2D();
		private var mPaddleHandle:uint;
		private var mIsGameplayBall:Boolean;
		private var mObjectTable:HandleTable;
		public function BallController( manager:GameManager, table:HandleTable, pawn:Actor, paddleID:uint, isGameplayBall:Boolean ) 
		{
			mPawn = pawn;
			keeper = manager;
			mPaddleHandle = paddleID;
			mPawn.mVelocity.setTo( Math.random() + .1, Math.random() + .5 );
			mPawn.mVelocity.normalize();
			mIsGameplayBall = isGameplayBall;
			mReducedPlayArea = new Rectangle( keeper.playArea.left + (mPawn.mDraw.width / 2.0), 
											 keeper.playArea.top + (mPawn.mDraw.height / 2.0), 
											 keeper.playArea.width - mPawn.mDraw.width,
											 keeper.playArea.height - mPawn.mDraw.height );
			mObjectTable = table;
		}
		
		// Are points A, B, and C supplied in counterclockwise order?
		private function ccw( A:Vector2D, B:Vector2D, C:Vector2D ):Boolean
		{
			return(((C.y-A.y)*(B.x-A.x)) > ((B.y-A.y)*(C.x-A.x)));
		}

		private function lineLineIntersectCheck( A:Vector2D, B:Vector2D, C:Vector2D, D:Vector2D ):Boolean
		{
			return((ccw(A,C,D) != ccw(B,C,D)) && (ccw(A,B,C) != ccw(A,B,D)));
		}
		
		// highly specialized because it is only used to perform one task. reusability is low, but 
		// that is because I don't project reusability to be of value to this functionality
		// based on http://bloggingmath.wordpress.com/2009/05/29/line-segment-intersection/
		private function shouldReflectY( newPos:Point ):Boolean
		{
			var padAct:Actor = Actor(mObjectTable.lookup(mPaddleHandle));
			var rectLines:Vector.<Line> = new Vector.<Line>();
			var startPoints:Vector.<Point> = new Vector.<Point>();
			var disp:Vector2D = new Vector2D();
			var res:Object = new Object(); // a 2-tuple representing the result of this function
			res.shouldReflect = false;
			res.distToIntersect = Number.POSITIVE_INFINITY;
			var punto:Point = mPawn.getPos();
			disp.x = newPos.x - punto.x;
			disp.y = newPos.y - punto.y;
			
			rectLines.push( new Line( padAct.mVolume.topLeft.x, 
									  padAct.mVolume.topLeft.y,
									  padAct.mVolume.topLeft.x,
									  padAct.mVolume.topLeft.y + padAct.mVolume.height ));
		    rectLines.push( new Line( padAct.mVolume.topLeft.x, 
									  padAct.mVolume.topLeft.y + padAct.mVolume.height,
									  padAct.mVolume.bottomRight.x,
									  padAct.mVolume.bottomRight.y ));
			rectLines.push( new Line( padAct.mVolume.bottomRight.x, 
									  padAct.mVolume.bottomRight.y,
									  padAct.mVolume.topLeft.x + padAct.mVolume.width,
									  padAct.mVolume.topLeft.y ));
		    rectLines.push( new Line( padAct.mVolume.topLeft.x + padAct.mVolume.width, 
									  padAct.mVolume.topLeft.y,
									  padAct.mVolume.topLeft.x,
									  padAct.mVolume.topLeft.y ));
									  
			startPoints.push( mPawn.mVolume.topLeft );
			startPoints.push( new Point( mPawn.mVolume.topLeft.x, mPawn.mVolume.topLeft.y + mPawn.mVolume.height ) ); 
			startPoints.push( mPawn.mVolume.bottomRight );
			startPoints.push( new Point( mPawn.mVolume.topLeft.x + mPawn.mVolume.width, mPawn.mVolume.topLeft.y ) );
			
			var startPoint:Vector2D;
			var endPoint:Vector2D = new Vector2D();
			
			for ( var i:int = 0; i < 4; i++ )
			{
				startPoint = Vector2D.toVect2D(startPoints[i]);
				endPoint = startPoint.add( disp );
				
				for ( var j:int = 0; j < 4; j ++ )
				{
					if ( lineLineIntersectCheck( startPoint, endPoint, Vector2D.toVect2D(rectLines[j].A), Vector2D.toVect2D(rectLines[j].B) ) )
					{
						var s:Vector2D = Vector2D.toVect2D(rectLines[j].B ).subtract(Vector2D.toVect2D( rectLines[j].A ));
						//s1 = p + t * r
						//s2 = q + u * s
						var numerator:Number = ( Vector2D.toVect2D( rectLines[j].A ).subtract( startPoint ).crossProduct( s ));
						var t:Number = numerator / disp.crossProduct( s );
						var pt:Point = new Point( startPoint.x + ( t * disp.x), startPoint.y + ( t * disp.y ) );
						if ( pt.length < res.distToIntersect )
						{
							res.distToIntersect = pt.length;
							res.shouldReflect = ( j % 2 ) == 1;
						}
					}
				}
			}
			return( res.shouldReflect );
		}
		
		public function tick( deltaTime:Number ):void
		{
			var padAct:Actor = Actor(mObjectTable.lookup(mPaddleHandle));
			var paddleY:Number = padAct.getPos().y - (padAct.mDraw.height / 2.);
			paddleY -= mPawn.mDraw.height / 2.;
			var newPos:Vector2D = new Vector2D( mPawn.getPos().x, mPawn.getPos().y );
			if ( Math.abs(mPawn.mVelocity.x) <= 0.01 )
			{
				mPawn.mVelocity.x += 0.1;
			}
			if ( mPawn.mVelocity.x + mPawn.mVelocity.y < 1.0 )
			{
				mPawn.mVelocity.normalize();
			}
			newPos.x += mPawn.mVelocity.x * moveSpeed * deltaTime;
			newPos.y += mPawn.mVelocity.y * moveSpeed * deltaTime;
			
			var oldpos:Point = mPawn.getPos();
			mPawn.setPos(newPos.x, newPos.y);
			
			if ( mIsGameplayBall )
			{
				if ( mPawn.mVolume.intersects( padAct.mVolume ) )
				{
					var npos:Point = new Point( newPos.x, newPos.y );
					mPawn.setPos( oldpos.x, oldpos.y );
					
					if ( shouldReflectY( npos ) )
					{
						mPawn.mVelocity.y *= -1.;
					}
					else
					{
						mPawn.mVelocity.x *= -1.;
					}
					
					mPawn.move( mPawn.mVelocity.x * moveSpeed * deltaTime * 2., mPawn.mVelocity.y * moveSpeed * deltaTime * 2. );
					keeper.score++;
					moveSpeed = Math.min( moveSpeed + RAMP_RATE, MAX_MOVE_SPEED );
					keeper.resources.playSfx("close");
					return;
				}
			}
			mPawn.setPos( oldpos.x, oldpos.y );
			
			if ( mReducedPlayArea.contains( newPos.x, newPos.y ) )
			{
				mPawn.setPos( newPos.x, newPos.y );
				return;
			}
			
			if ( (newPos.x >= mReducedPlayArea.right) || (newPos.x <= mReducedPlayArea.left) )
			{
				mPawn.mVelocity.x *= -1.;
				mPawn.move( mPawn.mVelocity.x * moveSpeed * deltaTime, mPawn.mVelocity.y * moveSpeed * deltaTime );
				keeper.resources.playSfx("far");
			}
			
			if ( (newPos.y <= mReducedPlayArea.top) )
			{
				mPawn.mVelocity.y *= -1.;
				mPawn.move( mPawn.mVelocity.x * moveSpeed * deltaTime, mPawn.mVelocity.y * moveSpeed * deltaTime );
				keeper.resources.playSfx("far");
			}
			
			if ( (newPos.y >= mReducedPlayArea.bottom) )
			{
				if ( mIsGameplayBall && !keeper.infinitePlay )
				{
					keeper.isGameOver = true;
				}
				else 
				{
					mPawn.mVelocity.y *= -1.;
					mPawn.move( mPawn.mVelocity.x * moveSpeed * deltaTime, mPawn.mVelocity.y * moveSpeed * deltaTime );
					keeper.resources.playSfx("far");
				}
			}
			
			var pt:Point = mPawn.getPos();
			var c:Point = new Point( keeper.playArea.x + (keeper.playArea.width / 2.),
										 keeper.playArea.y + (keeper.playArea.height / 2.));
			var correctionDir:Point = c.subtract( pt );
			while ( !keeper.playArea.intersects( mPawn.mDraw.getRect(mPawn.mDraw.parent)) )
			{	
				mPawn.mVelocity.setTo(correctionDir.x,  correctionDir.y );
				mPawn.mVelocity.normalize();
				mPawn.move( mPawn.mVelocity.x * moveSpeed * deltaTime * 2.0, mPawn.mVelocity.y * deltaTime * moveSpeed * 2.0 );
			}
		}
		
		public function toString():String
		{
			return "BallController";
		}
	}
}