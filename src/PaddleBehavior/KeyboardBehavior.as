package PaddleBehavior 
{
	import chaos.Actor;
	import chaos.IState;
	import chaos.ITickable;
	import flash.events.KeyboardEvent;
	import flash.ui.Keyboard;
	
	/**
	 * Makes the paddle be controlled by the left and right arrow keys
	 * @author Jeff Cochran
	 */
	public class KeyboardBehavior implements IState, ITickable 
	{
		private static const MOVE_SPEED:Number = 400.0;
		private var act:Actor;
		private var left:Number = 0.0;
		private var right:Number = 0.0;
		private var storedY:Number;
		private var distThresh:Number;
		private var speedCap:Number;
		public function KeyboardBehavior( actr:Actor, distThresh:Number, speedCap:Number  ) 
		{
			act = actr;
			storedY = -1.;
			this.distThresh = distThresh;
			this.speedCap = speedCap;
		}
		
		/* INTERFACE chaos.IState */
		
		public function setup():void 
		{
			left = right = 0.0;
			act.mDraw.stage.addEventListener( KeyboardEvent.KEY_DOWN, registerKeyEvent );
			act.mDraw.stage.addEventListener( KeyboardEvent.KEY_UP, registerKeyEvent );
		}
		
		public function teardown():void 
		{
			act.mDraw.stage.removeEventListener( KeyboardEvent.KEY_DOWN, registerKeyEvent );
			act.mDraw.stage.removeEventListener( KeyboardEvent.KEY_UP, registerKeyEvent );
		}
		
		public function registerKeyEvent( keyEvent:KeyboardEvent ):void
		{
			switch( keyEvent.keyCode )
			{
				case Keyboard.LEFT:
					left = ( keyEvent.type == KeyboardEvent.KEY_DOWN ) ? -1.0 : 0.0;
					break;
				case Keyboard.RIGHT:
					right = ( keyEvent.type == KeyboardEvent.KEY_DOWN ) ? 1.0 : 0.0;
					break;
				default:
					break;
			}
		}
		
		/* INTERFACE chaos.ITickable */
		
		public function tick(deltaTime:Number):void 
		{
			var xprop:Number;
			var xPos:Number = act.getPos().x + (MOVE_SPEED * deltaTime * (left + right));
			var instVel:Number = xPos - act.getPos().x;
			
			if ( storedY == -1.)
			{
				storedY = act.getPos().y;
			}
			
			act.setPos(xPos, storedY);
			if (xPos < (act.mDraw.stage.x + (act.mDraw.width / 2)))
			{
				act.mDraw.scaleX = 1.0;
				act.mDraw.scaleY = 1.0;
				act.setPos( (act.mDraw.stage.x + (act.mDraw.width / 2)), storedY );
			}
			else if (xPos > (act.mDraw.stage.stageWidth - (act.mDraw.width / 2)))
			{
				act.mDraw.scaleX = 1.0;
				act.mDraw.scaleY = 1.0;
				act.setPos( (act.mDraw.stage.stageWidth - (act.mDraw.width / 2)), storedY );
			}
			else if ( Math.abs(instVel) > distThresh ) // Only distend if we're going particularly fast
			{
				
				xprop = ( Math.abs(instVel) % speedCap )  / speedCap;
			
				act.mDraw.scaleX = 1.0 + xprop;
				act.mDraw.scaleY = 1.0 - xprop;
			}
			else
			{
				act.mDraw.scaleX = 1.;
				act.mDraw.scaleY = 1.;
			}
		}
	}
}