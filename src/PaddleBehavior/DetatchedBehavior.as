package PaddleBehavior 
{
	import chaos.Actor;
	import chaos.IState;
	import chaos.ITickable;
	import chaos.Vector2D;
	import flash.geom.Point;
	
	/**
	 * Paddle follows both x AND y of the mouse
	 * @author Jeff Cochran
	 */
	public class DetatchedBehavior implements ITickable, IState 
	{
		private var act:Actor;
		private var storedX:Number;
		private var storedY:Number;
		public function DetatchedBehavior( actr:Actor ) 
		{
			act = actr;
		}
		
		public function setup():void
		{
			storedX = act.getPos().x;
			storedY = act.getPos().y;
			act.mDraw.scaleX = 1.;
			act.mDraw.scaleY = 1.;
		}
		
		public function teardown():void
		{
			act.setPos( storedX, storedY );
		}
		
		public function tick( deltaTime:Number ):void
		{
			var vel:Vector2D = new Vector2D( act.mDraw.stage.mouseX - act.getPos().x,
											 act.mDraw.stage.mouseY - act.getPos().y );
			var newPos:Point = new Point( act.mDraw.stage.mouseX, act.mDraw.stage.mouseY );
			
			if (act.mDraw.stage.mouseX < (act.mDraw.stage.x + (act.mDraw.width / 2)))
			{
				newPos.x = act.mDraw.stage.x + (act.mDraw.width / 2);
			}
			else if (act.mDraw.stage.mouseX > (act.mDraw.stage.stageWidth - (act.mDraw.width / 2)))
			{
				newPos.x = act.mDraw.stage.stageWidth - (act.mDraw.width / 2);
			}
			
			if ( newPos.y < ( act.mDraw.stage.y + ( act.mDraw.height / 2 ) ) )
			{
				newPos.y = ( act.mDraw.stage.y + ( act.mDraw.height / 2 ) );
			}
			else if ( newPos.y > ( act.mDraw.stage.stageHeight - ( act.mDraw.height / 2 ) ) )
			{
				newPos.y = ( act.mDraw.stage.stageHeight - ( act.mDraw.height / 2 ) );
			}
			
			act.setPos(newPos.x, newPos.y);
		}
	}

}