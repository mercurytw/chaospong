package PaddleBehavior 
{
	import chaos.Actor;
	import chaos.IState;
	import chaos.ITickable;
	import flash.display.Sprite;
	
	/**
	 * Normal behavior for the paddle controller
	 * @author Jeff Cochran
	 */
	public class NormalBehavior implements IState, ITickable 
	{
		private var act:Actor;
		private var storedY:Number;
		private var distThresh:Number;
		private var speedCap:Number;
		public function NormalBehavior( actr:Actor, distThresh:Number, speedCap:Number ) 
		{
			act = actr;
			storedY = -1.;
			this.distThresh = distThresh;
			this.speedCap = speedCap;
		}
		
		public function setup():void
		{
			
		}
		
		public function teardown():void
		{
			
		}
		
		public function tick( deltaTime:Number ):void
		{
			var xprop:Number;
			var instVel:Number = act.mDraw.stage.mouseX - act.getPos().x;
			
			if ( storedY == -1.)
			{
				storedY = act.getPos().y;
			}
			
			act.setPos(act.mDraw.stage.mouseX, storedY);
			if (act.mDraw.stage.mouseX < (act.mDraw.stage.x + (act.mDraw.width / 2)))
			{
				act.mDraw.scaleX = 1.0;
				act.mDraw.scaleY = 1.0;
				act.setPos( (act.mDraw.stage.x + (act.mDraw.width / 2)), storedY );
			}
			else if (act.mDraw.stage.mouseX > (act.mDraw.stage.stageWidth - (act.mDraw.width / 2)))
			{
				act.mDraw.scaleX = 1.0;
				act.mDraw.scaleY = 1.0;
				act.setPos( (act.mDraw.stage.stageWidth - (act.mDraw.width / 2)), storedY );
			}
			else if ( Math.abs(instVel) > distThresh ) // Only distend if we're going particularly fast
			{
				
				xprop = ( Math.abs(instVel) % speedCap )  / speedCap;
			
				act.mDraw.scaleX = 1.0 + xprop;
				act.mDraw.scaleY = 1.0 - xprop;
			}
			else
			{
				act.mDraw.scaleX = 1.;
				act.mDraw.scaleY = 1.;
			}
		}
		
	}

}